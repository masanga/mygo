<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Services extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId');
            $table->string('status')->nullable();
            $table->string('serviceCategory')->nullable();
            $table->string('serviceTitle');
            $table->longText('serviceInfo')->nullable();
            
            $table->string('bronze_price_a_1')->nullable();
            $table->string('bronze_currency_a_1')->nullable();
            $table->string('bronze_description_a_1')->nullable();
            $table->string('bronze_price_a_2')->nullable();
            $table->string('bronze_currency_a_2')->nullable();
            $table->string('bronze_description_a_2')->nullable();

            $table->string('silver_price_a_1')->nullable();
            $table->string('silver_currency_a_1')->nullable();
            $table->string('silver_description_a_1')->nullable();
            $table->string('silver_price_a_2')->nullable();
            $table->string('silver_currency_a_2')->nullable();
            $table->string('silver_description_a_2')->nullable();

            $table->string('gold_price_a_1')->nullable();
            $table->string('gold_currency_a_1')->nullable();
            $table->string('gold_description_a_1')->nullable();
            $table->string('gold_price_a_2')->nullable();
            $table->string('gold_currency_a_2')->nullable();
            $table->string('gold_description_a_2')->nullable();

            $table->string('diamond_price_a_1')->nullable();
            $table->string('diamond_currency_a_1')->nullable();
            $table->string('diamond_description_a_1')->nullable();
            $table->string('diamond_price_a_2')->nullable();
            $table->string('diamond_currency_a_2')->nullable();
            $table->string('diamond_description_a_2')->nullable();

            $table->string('serviceImage1')->nullable();
            $table->string('serviceImage2')->nullable();
            $table->string('serviceImage3')->nullable();
            $table->string('serviceImage4')->nullable();
            $table->string('serviceImage5')->nullable();
            $table->string('serviceImage6')->nullable();
            $table->string('serviceImage7')->nullable();
            $table->string('serviceImage8')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
