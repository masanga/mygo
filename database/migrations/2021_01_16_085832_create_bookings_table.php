<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('bookingStatus')->default('NEW');

            $table->string('userId')->nullable();
            $table->string('agentId')->nullable();
            $table->string('serviceSelectedId')->nullable();
            $table->string('audioFileUploaded')->nullable();
            $table->string('serviceDateBooking')->nullable();

            $table->string('numberOfPrice1')->nullable();
            $table->string('numberOfPrice2')->nullable();

            $table->string('nameOfBooking')->nullable();
            $table->string('phoneOfBooking')->nullable();
            $table->string('totalPrice')->nullable();
            $table->string('totalPriceCurrency')->nullable();
            $table->string('depositPaymentPrice')->nullable();
            $table->string('depositPaymentCurrency')->nullable();

            $table->string('restPrice')->nullable();
            $table->string('restCurrency')->nullable();
            $table->string('notes')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
