<?php

use App\Exports\BookingExport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'Controller@index',
						'as' => 'index']);

//Dashboard
Route::get('/dashboard', 'HomeController@index')->name('home');

Auth::routes();

Route::middleware('forceSSL')->group(function () {

});

//Protected Routes
Route::middleware(['auth'])->group(function () {

	//Download Pdf
	Route::get('download/pdf', ['uses' => 'ReportController@downloadPdf','as' => 'download']);

	//Searching Agent name
	Route::get('/get/agent/names', ['uses' => 'ReportController@searchAgentName']);

	//Searching Similar Booking Names
	Route::get('/get/booking/similar/names', ['uses' => 'BookingController@searchSimilarNameOnBooking']);

	//Get Generate Bookings Report View
	Route::get('/generate/bookings/report/view', ['uses' => 'ReportController@generateBookingsReportView',
													'as' => 'generateBookingsReportView']);

	//Get Generate Agents Report View 
	Route::get('/generate/agents/report/view', ['uses' => 'ReportController@generateAgentsReportView',
												  'as' => 'generateAgentsReportView']);

	//Get Generate Services Report View
	Route::get('/generate/services/report/view', ['uses' => 'ReportController@generateServicesReport',
												'as' => 'generateServicesReportView']);

    //Get Generate Services Report View
	Route::get('/services/report/view', ['uses' => 'ReportController@servicesReportView',
												'as' => 'servicesReportView']);

	//Audio Upload Test
	Route::post('/audio/upload/test', ['uses' => 'BookingController@audioUploadTest',
										'as' => 'audio-upload-test']);

	//Get Service Data
	Route::get('/get/service/details/{serviceId}', ['uses' => 'ServiceController@getServiceDetails']);

	//Posting New Booking
	Route::post('/post/new/bookings', ['uses' => 'BookingController@postNewBooking',
										'as' => 'post-new-booking']);

	//Generate Bookings Report
	Route::get('/generate/csv/report', ['uses' => 'ReportController@generateCsvReport',
											   'as' => 'csv-reports']);

	//Generate Services Report
	Route::get('/generate/excel/report', ['uses' => 'ReportController@generateExcelReport',
											   'as' => 'excel-reports']);

	//Generate Agents Report
	Route::get('/generate/pdf/reports', ['uses' => 'ReportController@generatePdfReport',
											  'as' => 'pdf-reports']);

	//Audio Record Test
	Route::get('/audio/record/test', ['uses' => 'BookingController@audioRecordTest']);

	//Add Service to Agent
	Route::get('/add/service/to/agent/{agentId}/{serviceId}', ['uses' => 'ServiceController@addServiceToAgent']);

	//Remove Service to Agent
	Route::get('/remove/service/to/agent/{agentId}/{serviceId}', ['uses' => 'ServiceController@removeServiceToAgent']);

	//Admin Bookings Viewer
	Route::get('/admin/bookings', ['uses' => 'Controller@adminBookings',
							   'as' => 'admin-bookings']);

	//Admin Agenr Services
	Route::get('/admin/agent/services/list/{agentId}', ['uses' => 'ServiceController@agentServicesList']);

	//Update Service
	Route::post('/postUpdateServiceData', ['uses' => 'ServiceController@postUpdateServiceData',
											 'as' => 'postUpdateServiceData']);

	//Delete Service
	Route::get('/admin/delete/service/{serviceId}', ['uses' => 'ServiceController@deleteService']);

	//Update Category
	Route::post('/postUpdateCategory', ['uses' => 'CategoryController@postUpdateCategory',
										  'as' => 'postUpdateCategory']);

	//Delete Category
	Route::get('/admin/delete/category/{categoryId}', ['uses' => 'CategoryController@deleteCategory']);

	//postNewCategory
	Route::post('/postNewCategory', ['uses' => 'CategoryController@postNewCategory',
									   'as' => 'postNewCategory']);

	//Categories
	Route::get('/categories', ['uses' => 'CategoryController@categories',
								'as' => 'categories']);

	//Post New Tour
	Route::post('/postNewService', ['uses' => 'ServiceController@postNewService',
								   'as' => 'postNewService']);

	//Delete Agent Info
	Route::get('/admin/delete/agent/{agentId}', ['uses' => 'Controller@deleteAgentInfo']);

	//Update Agent Info
	Route::post('/postUpdateAgentDetails', ['uses' => 'Controller@postUpdateAgentDetails',
											  'as' => 'postUpdateAgentDetails']);

	//Admin Routes
	Route::post('/postNewAgent', ['uses' => 'Controller@postNewAgent',
									'as' => 'postNewAgent']);



	//Post New Booking
	Route::post('/postNewBooking', ['uses' => 'BookingController@postNewBooking',
									  'as' => 'postNewBooking']);

	//Change User Password
	Route::post('/postChangePassword', ['uses' => 'Controller@postChangePassword',
										'as' => 'postChangePassword']);
	

	//Login Logs
	Route::get('/login/logs', ['uses' => 'Controller@loginLogs',
								'as' => 'login-logs']);

	//Change Password
	Route::get('/change/password', ['uses' => 'Controller@changePassword',
									  'as' => 'change-password']);

	//Reports
	Route::get('/reports', ['uses' => 'Controller@reports',
							   'as' => 'reports']);

	//Bookings
	Route::get('/bookings', ['uses' => 'Controller@bookings',
							   'as' => 'bookings']);

	//Tours
	Route::get('/services', ['uses' => 'Controller@services',
							'as' => 'services']);

	//Agents
	Route::get('/agents', ['uses' => 'Controller@agents',
							'as' => 'agents']);

	
	Route::get('/download', function() {
	    return Excel::download(new BookingExport, 'bookings.xlsx');
	});

});

