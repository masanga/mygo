<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<style type="text/css">
	table {
		width: 100%;
	}

	table tr td {
		padding-top: 16px;
	}

	.left {
		text-align: left;
	}

	.center {
		text-align: center;
	}

	.right {
		text-align: right;
	}

	.red {
		color: red;
	}

	.green {
		color: red;
	}

	.yellow {
		color: yellow;
	}

	.padding-normal {
		padding: 1%;
	}

	.well-borders {
		border: 2px solid #eee;
		border-radius: 5px;
	}

	fieldset.scheduler-border {
	    border: 1px groove #ddd !important;
	    border-radius: 5px;
	    padding: 0 1.4em 1.4em 1.4em !important;
	    margin: 0 0 1.5em 0 !important;
	    -webkit-box-shadow:  0px 0px 0px 0px #000;
	            box-shadow:  0px 0px 0px 0px #000;
	}

	legend.scheduler-border {
	    width:inherit; /* Or auto */
	    padding:0 10px; /* To give a bit of padding on the left and right */
	    border-radius: 5px;
	    border: 1px solid green;
	}
</style>

<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7"></script>

@if (Session::Has('successMessage'))

	<script type="text/javascript">
		Swal.fire(
		  'Success',
		  '{{ Session::get('successMessage') }}',
		  'success'
		);
	</script>

@elseif (Session::Has('errorMessage')) 

	
	<script type="text/javascript">
		Swal.fire(
		  'Error',
		  '{{ Session::get('errorMessage') }}',
		  'error'
		);
	</script>


@endif
