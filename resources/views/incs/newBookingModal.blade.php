@php

use App\User;
use App\Service;

$agentsList = User::where('level', 2)->get();
$serviceList = Service::where('status', 'TRUE')->get();

@endphp

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

{{-- <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script> --}}

{{-- <script type="text/javascript" src="{{ asset("$pF/js/pickadate_js/lib/picker.js") }}"></script>

<script type="text/javascript" src="{{ asset("$pF/js/pickadate_js/lib/picker.date.js") }}"></script>

<script type="text/javascript" src="{{ asset("$pF/js/pickadate_js/lib/picker.time.js") }}"></script>

<script type="text/javascript" src="{{ asset("$pF/js/pickadate_js/lib/legacy.js") }}"></script>

<link rel="stylesheet" type="text/css" href="{{ asset("$pF/js/pickadate_js/lib/themes/classic.css") }}"> --}}

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
{{-- <script src="//code.jquery.com/jquery-1.12.4.js"></script> --}}
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
    .box{
        position: relative;
        display: inline-block; /* Make the width of box same as image */
    }
    .box .text{
        position: absolute;
        z-index: 999;
        margin: 0 auto;
        left: -50;
        right: 0;
        top: 30%; /* Adjust this value to move the positioned div up and down */
        text-align: center;
        width: 40%; /* Set the width of the positioned div */
        color: white;
        text-shadow: 2px 2px #000000;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="newBookingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">New Booking</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
    
        @if (Auth::User()->level == 1)

        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Agent</legend>

              <div class="row">
                @foreach($agentsList as $agent)
                  <button class="btn btn-primary" style="margin: 1.5%;" onclick="setAgentOnForm('{{ $agent->id }}', '{{ $agent->firstName }} {{ $agent->lastName }}')">{{ $agent->firstName }} {{ $agent->lastName }}</button>
                @endforeach
              </div>

              <br/>

              <div class="col-md-12 center padding-normal well-borders">
                <span id="agentSelectedView">No Agent Selected</span>
              </div>
          
        </fieldset>

        @endif

        {{-- Sound Record --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Sound Record</legend>

            <div class="row">
                
              <div class="col-md-12">
                <p> 
                  <button class="btn btn-primary form-control" id="btnStart" style="margin: 1.5%;">START RECORDING</button> 
                       
                  <button class="btn btn-danger form-control" id="btnStop" style="margin: 1.5%;">STOP RECORDING</button> 
                  <!--button for 'stop recording'--> 
                </p>     
              </div>  
            
              <div class="col-md-12">
                <p>
                    <!--for record--> 
                    <audio controls id="hideAudio"></audio> 
                    <!--'controls' use for add 
                      play, pause, and volume--> 

                    <!--for play the audio--> 
                    <audio id="adioPlay" controls class="form-control"></audio> 
                  
                </p>    
              </div>
              
            </div>

            <br/>

            <div class="col-md-12 center padding-normal well-borders" id="recordStatusView">
              <span id="recordStatusV">No Record Recorded</span>
            </div>
        
        </fieldset>
        {{-- Sound Record --}}


        {{-- Service  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Service</legend>

              <div class="row">
                @foreach($serviceList as $service)
                  {{-- <button class="btn btn-primary" style="margin: 1.5%;" ></button> --}}

                  <div class="col-md-4 box">
                    <img src="{{ asset("$pF/storage/services/$service->id/1.jpg") }}" style="width: 200px; height: 200px;" onclick="setServiceSelectedForm('{{ $service->id }}', '{{ $service->serviceTitle }}');" class="img img-rounded">  
                    
                    <div class="text" onclick="setServiceSelectedForm('{{ $service->id }}', '{{ $service->serviceTitle }}');">
                        <h3>{{ $service->serviceTitle }}</h3>
                    </div>
                  </div>
                  

                @endforeach
              </div>

              <br/>

              <div class="col-md-12 center padding-normal well-borders">
                <span id="serviceSelectedView">No Service Selected</span>
              </div>
          
        </fieldset>
        {{-- Service  --}}

        {{-- Service  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Date</legend>


              <div class="container">
                 <div class="row">
                    <div class='col-md-12'>
                          {{-- <input type='text' class="form-control" id="datePickerJS" /> --}}
                         <p>Date: <input type="text" id="datepicker2" disabled></p>
                         
                          <div id="datepicker"></div>

                        
                    </div>
                  </div>
             </div>
        
        </fieldset>
        {{-- Service  --}}



        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Number of Price 1</legend>

              <div class="row">
                
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('1');">1</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('2');">2</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('3');">3</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('4');">4</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('5');">5</button>


                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('6');">6</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('7');">7</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('8');">8</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('9');">9</button>
                <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice1('10');">10</button>


              </div>

              <input type="number" name="numberOfPrice1Input" id="numberOfPrice1Input" class="form-control" placeholder="Number of Price 1">

              <br/>

              <div class="col-md-12 center padding-normal well-borders" id="totalNumberofPrice1">
                <span>Total</span>
              </div>
          
        </fieldset>


        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Number of Price 2</legend>

           <div class="row">
             
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('1');">1</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('2');">2</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('3');">3</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('4');">4</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('5');">5</button>


             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('6');">6</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('7');">7</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('8');">8</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('9');">9</button>
             <button class="btn btn-primary" style="width: 60px; padding: 1.5%; margin: 1.5%;" onclick="setNumberOfPrice2('10');">10</button>


           </div>

           <input type="number" name="numberOfPrice2Input" id="numberOfPrice2Input" class="form-control" placeholder="Number of Price 2">

           <br/>

           <div class="col-md-12 center padding-normal well-borders" id="totalNumberofPrice2">
             <span>Total</span>
           </div>
          
        </fieldset>


        {{-- Name  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Name</legend>

           <input type="text" name="name" placeholder="Enter Name" class="form-control" id="bookingNameId" autocomplete="off" >

           <br/>

           <ul style="text-decoration: none; list-style: none;" id="searchingNameResultsTab">
             
           </ul>
          
        </fieldset>
        {{-- Name --}}


        {{-- Phone Number  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Phone Number</legend>

           <input type="tel" name="phone" placeholder="Enter Phone Number" class="form-control" id="phoneNumberId">
          
        </fieldset>
        {{-- Phone Number --}}


        {{-- Phone Number  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Total Price</legend>

           <input type="number" name="totalPrice" placeholder="Total Price" class="form-control" id="totalPriceId">
          
            <br/>
            <div class="row">
                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setTotalPriceCurrency('TL');">TL</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setTotalPriceCurrency('$');">$</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setTotalPriceCurrency('€');">€</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setTotalPriceCurrency('£');">£</button>
                </div>
              
            </div>

            <br/>
            <div class="col-md-12 center padding-normal well-borders">
              <span id="totalPriceView">Total Price : </span>
            </div>
            

        </fieldset>
        {{-- Phone Number --}}




        {{-- Deposit Amount  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Deposit Amount</legend>

           <input type="number" name="depositAmount" placeholder="Enter Deposit Amount" class="form-control" id="depositAmountId">
          

            <br/>
            <div class="row">
                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setDepositAmountCurrency('TL');">TL</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setDepositAmountCurrency('$');">$</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setDepositAmountCurrency('€');">€</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setDepositAmountCurrency('£');">£</button>
                </div>
              
            </div>

            <br/>
            <div class="col-md-12 center padding-normal well-borders">
              <span id="depositAmountView">Deposit Amount : </span>
            </div>

        </fieldset>
        {{-- Deposit Amount --}}


        {{-- Rest Amount  --}}
        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Rest Amount</legend>

           <input type="number" name="restAmount" placeholder="Enter Rest Amount" class="form-control" id="restAmountId">
          

            <br/>
            <div class="row">
                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setRestAmountCurrency('TL');">TL</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setRestAmountCurrency('$');">$</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setRestAmountCurrency('€');">€</button>
                </div>

                <div class="col-md-3">
                  <button class="btn btn-primary form-control" onclick="setRestAmountCurrency('£');">£</button>
                </div>
              
            </div>

            <br/>
            <div class="col-md-12 center padding-normal well-borders">
              <span id="restAmountView">Rest Amount : </span>
            </div>

        </fieldset>
        {{-- Rest Amount  --}}


        {{-- Notes  --}}
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Notes</legend>

           
           <textarea class="form-control" id="notesId" placeholder="Notes"></textarea>
          
        </fieldset>
        {{-- Notes  --}}

        <hr/>
        <button class="btn btn-success form-control" id="submitBookingButton">Submit Booking</button>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
 
    // $('#datepicker').datepicker();
    $('#datepicker').datepicker({dateFormat: 'yy-mm-dd', altField: '#datepicker2'});
  

  
    // $("#datepicker2").datepicker();

</script>


<script type="text/javascript">

  let audioData;

  var agentId = "{{ Auth::User()->id }}";
  var serviceSelectedId = "";
  var serviceDateBooking = $("#datepicker2").val();
  var numberOfPrice1 = "";
  var numberOfPrice2 = "";
  var nameOfBooking = "";
  var phoneOfBooking = "";

  var totalPrice = "";
  var totalPriceCurrency = "";

  var depositPaymentPrice = "";
  var depositPaymentCurrency = "";

  var restPrice = "";
  var restCurrency = "";

  var notes = "";


  var servicePrice1 = "";
  var servicePrice1Currency = "";

  var servicePrice2 = "";
  var servicePrice2Currency = "";

  var total1Price = 0;
  var total2Price = 0;


  $("#restAmountId").keyup(function() {

    simulateRestPriceAction();

  });

  function calculateDepositPrice()
  {
     var totalPriceTemp = parseInt($("#totalPriceId").val());
     var depositPriceTemp = parseInt($("#depositAmountId").val());

     $("#restAmountId").val(totalPriceTemp - depositPriceTemp);
  }


  function calculateTotalPrice(total1Price, total2Price)
  {
    totalPrice = parseInt(total1Price) + parseInt(total2Price);

    $("#totalPriceId").val(totalPrice);
  }


  function simulateRestPriceAction()
  {
      restPrice = $("#restAmountId").val();

      $("#restAmountView").html("Rest Price : " + restPrice + " " + restCurrency); 
  }

  function setRestAmountCurrency(currency)
  {
    restCurrency = currency;

    simulateRestPriceAction();
  }


  $("#depositAmountId").keyup(function() {

    simulateDepositPriceAction();

    calculateDepositPrice();

  });

  function setDepositAmountCurrency(currency)
  {
    depositPaymentCurrency = currency;

    simulateDepositPriceAction();
  }

  function simulateDepositPriceAction()
  {
    depositPaymentPrice = $("#depositAmountId").val();

    $("#depositAmountView").html("Deposit Price : " + depositPaymentPrice + " " + depositPaymentCurrency);
  }


  $("#totalPriceId").keyup(function() {

    simulateTotalPriceAction();

  });


  function simulateTotalPriceAction()
  {
    totalPrice = $("#totalPriceId").val();

    $("#totalPriceView").html("Total Price : " + totalPrice + " " + totalPriceCurrency);
  }


  //Set Total Price Currency
  function setTotalPriceCurrency(currency)
  {
    totalPriceCurrency = currency;

    simulateTotalPriceAction();
  }

  //Setting Number og Price 1
  function setNumberOfPrice2(number)
  {
    numberOfPrice2 = number;

    total2Price = parseInt(servicePrice2) * parseInt(number);

    $("#numberOfPrice2Input").val(number);

    $("#totalNumberofPrice2").html("<strong> Total : " + number + " - Price : "+ total2Price + " " + servicePrice2Currency + " </strong>");

    //Calculate Total Price
    calculateTotalPrice(total1Price, total2Price);

    calculateDepositPrice();

  }


  $("#numberOfPrice2Input").keyup(function () {

    var price2Edited = parseInt($("#numberOfPrice2Input").val());

    setNumberOfPrice2(price2Edited);

  });


  //Setting Number og Price 1
  function setNumberOfPrice1(number)
  {
    numberOfPrice1 = number;

    total1Price = parseInt(servicePrice1) * parseInt(number);

    $("#numberOfPrice1Input").val(number);

    $("#totalNumberofPrice1").html("<strong> Total : " + number + " - Price : "+ total1Price + " " + servicePrice1Currency + " </strong>");

    //Calculate Total Price
    calculateTotalPrice(total1Price, total2Price);

    calculateDepositPrice();

  }

  $("#numberOfPrice1Input").keyup(function () {

    var price1Edited = parseInt($("#numberOfPrice1Input").val());

    setNumberOfPrice1(price1Edited);

  });

  //Set Booking Name
  function setBookingName(nameChosen)
  {
    $("#searchingNameResultsTab").html("");

    $("#bookingNameId").val(nameChosen);

    console.log(nameChosen);
  }

  //Searching if User Exists in Database
  $("#bookingNameId").keyup(function() {

      var query = $("#bookingNameId").val();

      $.get("{{ goPL() }}/get/booking/similar/names?nameQuery="+query, function (data) {

        var jsonData = JSON.parse(JSON.stringify(data));

        var data = jsonData.data;

        $("#searchingNameResultsTab").html("");

        data.forEach(function(nameLike) {

          $("#searchingNameResultsTab").append(`<li style="background-color: blue; color:white; border-radius: 15px; border: 1px solid #eee; 
            padding: 20px; margin-bottom: 5px;" onclick="setBookingName('`+nameLike+`')">`+nameLike+`</li>`);

        });

      });

  });

  //Alerting Agent Id When Admin
  function setAgentOnForm(agentIdSelected, agentFullName)
  {
    agentId = agentIdSelected;
    console.log("Agent Id : " + agentId);
    $("#agentSelectedView").html("Agent Selected : " + agentFullName);
  }

  //On Service Selected Id
  function setServiceSelectedForm(serviceId, serviceFullName)
  {
      serviceSelectedId = serviceId;
      $("#serviceSelectedView").html("Service Selected : " + serviceFullName);


      $.get('{{ goPL() }}/get/service/details/'+serviceId, function (data) {

        var jsonData = JSON.parse(JSON.stringify(data));

        servicePrice1 = jsonData['0'].bronze_price_a_1;
        servicePrice1Currency = jsonData['0'].bronze_currency_a_1;

        servicePrice2 = jsonData['0'].bronze_price_a_2;
        servicePrice2Currency = jsonData['0'].bronze_currency_a_2;

        console.log("Price : " + servicePrice1 + " Currency : " + servicePrice1Currency);

      });

  }


  $("#submitBookingButton").click(function () {

    serviceDateBooking = $("#datepicker2").val();

    nameOfBooking = $("#bookingNameId").val();
    phoneOfBooking = $("#phoneNumberId").val();

    totalPriceId = $("#totalPriceId").val();

    notes = $("#notesId").val();

    var formData = new FormData();
    formData.append('audio', audioData);

    formData.append('agentId', agentId);
    formData.append('serviceSelectedId', serviceSelectedId);
    formData.append('serviceDateBooking', serviceDateBooking);
    formData.append('numberOfPrice1', numberOfPrice1);
    formData.append('numberOfPrice2', numberOfPrice2);
    formData.append('nameOfBooking', nameOfBooking);
    formData.append('phoneOfBooking', phoneOfBooking);
    formData.append('totalPrice', totalPrice);
    formData.append('totalPriceCurrency', totalPriceCurrency);
    formData.append('depositPaymentPrice', depositPaymentPrice);

    formData.append('depositPaymentCurrency', depositPaymentCurrency);
    formData.append('restPrice', restPrice);
    formData.append('restCurrency', restCurrency);

    formData.append('notes', notes);
    formData.append('_token', "{{ Session::token() }}");
    
    $.ajax({
      type: 'POST',
      enctype: 'multipart/form-data',
      url: '{{ route('post-new-booking') }}',
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function (data) {
        console.log(data);

        var jsonData = JSON.parse(JSON.stringify(data));

        if (jsonData.status == "SUCCESS") {
          Swal.fire(
            'Success',
            'New Booking Was Added Successfully',
            'success'
          ).then(function() {
            window.location.reload();
          });
        }
      },
      error: function (err) {
        console.log(err);
      }

    });

  });

</script>



<script> 

$(document).ready(function () {

  $("#hideAudio").hide();

  let audioIN = { audio: true }; 

  navigator.mediaDevices.getUserMedia(audioIN) 

  .then(function (mediaStreamObj) { 

    let audio = document.querySelector('audio'); 
     
    if ("srcObject" in audio) { 
    audio.srcObject = mediaStreamObj; 
    } 
    else { // Old version 
    audio.src = window.URL 
      .createObjectURL(mediaStreamObj); 
    } 

    audio.onloadedmetadata = function (ev) { 

    }; 

    let start = document.getElementById('btnStart'); 

    let stop = document.getElementById('btnStop'); 

    let playAudio = document.getElementById('adioPlay'); 

    let mediaRecorder = new MediaRecorder(mediaStreamObj); 

    start.addEventListener('click', function (ev) { 
        mediaRecorder.start(); 
        console.log(mediaRecorder.state); 

        $("#recordStatusV").html("Recording...");
    }) 

    stop.addEventListener('click', function (ev) { 
        mediaRecorder.stop(); 

        $("#recordStatusV").html("Recording Saved");
    
    }); 

    mediaRecorder.ondataavailable = function (ev) { 
    dataArray.push(ev.data); 
    } 

    let dataArray = []; 

    mediaRecorder.onstop = function (ev) { 

    audioData = new Blob(dataArray, 
          { 'type': 'audio/ogg' }); 
      
    dataArray = []; 



    let audioSrc = window.URL 
      .createObjectURL(audioData); 

    playAudio.src = audioSrc; 
    } 
  }) 

  .catch(function (err) { 
    console.log(err.name, err.message); 
  }); 

});

  
</script> 




