<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" type="text/css" href="{{ ("$pF/bootstrap/css/bootstrap.min.css") }}">
<!-- Optional theme -->
<link rel="stylesheet" href="{{ ("$pF/bootstrap/css/bootstrap-theme.min.css") }}">
<!-- Latest compiled and minified JavaScript -->
<script type="text/javascript" src="{{ ("$pF/bootstrap/js/bootstrap.min.js") }}"></script>
<style type="text/css">

.pagination {
	float: right;
}

.bordered-form {
	border-radius: 2px;
	border: 2px solid #eeeeee;
	padding:1.5%;
}

table {
	width: 100%;
}

table tr td {
	padding-top: 15px;
}

body h2 h3 p div {
	font-weight: lighter;
}

.row {
	margin: 0%;
}

#bs-example-navbar-collapse-1 ul li a {
	color: white;
}

#bs-example-navbar-collapse-1 ul li a:hover {
	background-color: #{{ env('CMS_COLOR_2') }};
	color: white;
	border-radius: 0px;
}

.navbar-header .navbar-brand {
	color: white;
}

.btn-primary {
	background-color: #ffffff;
	color: black;
	border: 2px solid #{{ env('CMS_COLOR_1') }};
	border-radius: 2px;
}

.btn-primary:hover {
	background-color: #{{ env('CMS_COLOR_1') }};
	color: white;
	border: 2px solid #{{ env('CMS_COLOR_1') }};
	border-radius: 2px;
}

.btn-info {
	background-color: #ffffff;
	color: black;
	border: 2px solid #5bc0de;
	border-radius: 2px;
}

.btn-success {
	background-color: #ffffff;
	color: black;
	border: 2px solid #5cb85c;
	border-radius: 2px;
}

.btn-danger {
	background-color: #ffffff;
	color: black;
	border: 2px solid #d9534f;
	border-radius: 2px;
}

.div-flex {
	 display: flex;
}

.text-center {
	text-align: center;
}

.text-left {
	text-align: left;
}

.text-right {
	text-align: right;
}

.left {
	text-align: left;
}

.center {
	text-align: center;
}

.right {
	text-align: right;
}

.float-left {
	float: left;
}

.float-right {
	float: right;
}

.div-flex {
	display: inline-flex;
}

.default-table-size {
	width: 100%;
}

.no-margin {
	margin-left: 0%;
	margin-right: 0%;	
	margin-top: 0%;
	margin-bottom: 0%;
}

.documentation_left_header ul {
	list-style: none;
}

.swal-wide{
    width:850px !important;
}

fieldset.scheduler-border {
    border: 1px groove black !important;
    border-radius: 5px;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
    font-size: 1.2em !important;
    font-weight: bold !important;
    text-align: left !important;
    width:auto;
    padding:0 10px;
    border-bottom:none;
}

</style>