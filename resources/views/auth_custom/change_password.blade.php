@extends('layouts.adminlte')

@section('pageTitle') {{ "Change Password" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Change Password</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Change Password</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

<div class="container-fluid">

  <div class="col-md-9">
    

      <form action="{{ route('postChangePassword') }}" method="POST">
        
          <table>
            <tr>
              <td>Current Password</td>
              <td><input type="password" name="current_password" placeholder="Enter Current Password" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>New Password</td>
              <td><input type="password" name="new_password" placeholder="Enter New Password" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>New Password Confirmation</td>
              <td><input type="password" name="new_password_confirmation" placeholder="Enter New Password Confirmation" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td><input type="hidden" name="_token" value="{{ Session::token() }}"></td>
              <td><input type="submit" class="btn btn-primary form-control"></td>
            </tr>
          </table>

      </form>

  </div>

</div>

@endsection
