@extends('layouts.adminlte')

@section('pageTitle') {{ "Login Logs" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Login Logs</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Login Logs</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

<div class="container-fluid">
  
<table class="table table-bordered">
  <tr>
    <td>ID</td>
    <td>Operating System</td>
    <td>Browser</td>
    <td>IP</td>
    <td>Last Login</td>
  </tr>

  @php $id = 1; @endphp
  @foreach($loginLogs as $loginLog)
    <tr>
      <td>{{ $id++ }}</td>
      <td>{{ $loginLog->os }}</td>
      <td>{{ $loginLog->browser }}</td>
      <td>{{ $loginLog->ip }}</td>
      <td>{{ $loginLog->created_at->diffForHumans() }}</td>
    </tr>
  @endforeach
</table>

<div class="pagination">
  {{ $loginLogs->links() }}
</div>

</div>

@endsection
