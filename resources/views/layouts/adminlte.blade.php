<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('pageTitle') | {{ $appName }}</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/fontawesome-free/css/all.min.css") }}">
  
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css") }}">
  
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css") }}">
  
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/jqvmap/jqvmap.min.css") }}">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset("$pF/template/dist/css/adminlte.min.css") }}">
  
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/overlayScrollbars/css/OverlayScrollbars.min.css") }}">
  
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/daterangepicker/daterangepicker.css") }}">
  
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset("$pF/template/plugins/summernote/summernote-bs4.min.css") }}">

  {{-- Jquery CDN --}}
  <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{ route('home') }}" class="nav-link">Home</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">

      <!-- Messages Dropdown Menu -->
      
      <!-- Notifications Dropdown Menu -->
      
      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
      
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('home') }}" class="brand-link">
      <span class="brand-text font-weight-light">&nbsp;&nbsp;{{ $appName }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        
        <div class="info">
          <a href="#" class="d-block">{{ Auth::User()->firstName }} {{ Auth::User()->lastName }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="{{ route('home') }}" class="nav-link @if(Route::currentRouteName() == "home") {{ "active" }} @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>       
          </li>

          @if (Auth::User()->level == 1)
            <li class="nav-item">
              <a href="{{ route('agents') }}" class="nav-link @if(Route::currentRouteName() == "agents") {{ "active" }} @endif">
                <i class="nav-icon fa fa-users"></i>
                <p>
                  Agents
                </p>
              </a>       
            </li>

            <li class="nav-item">
              <a href="{{ route('categories') }}" class="nav-link @if(Route::currentRouteName() == "categories") {{ "active" }} @endif">
                <i class="fa fa-linode" aria-hidden="true"></i>
                <p>
                  Service Category
                </p>
              </a>       
            </li>

            <li class="nav-item">
              <a href="{{ route('services') }}" class="nav-link @if(Route::currentRouteName() == "services") {{ "active" }} @endif">
                <i class="nav-icon fa fa-cubes"></i>
                <p>
                  Services
                </p>
              </a>       
            </li>
          @endif


          @if (Auth::User()->level == 1)

            <li class="nav-item">
              <a href="{{ route('admin-bookings') }}" class="nav-link @if(Route::currentRouteName() == "admin-bookings") {{ "active" }} @endif">
                <i class="nav-icon fa fa-book"></i>
                <p>
                  Bookings
                </p>
              </a>       
            </li>

          @elseif (Auth::User()->level == 2) 

            <li class="nav-item">
              <a href="{{ route('bookings') }}" class="nav-link @if(Route::currentRouteName() == "bookings") {{ "active" }} @endif">
                <i class="nav-icon fa fa-book"></i>
                <p>
                  Bookings
                </p>
              </a>       
            </li>

          @endif
          

          <li class="nav-item">
            <a href="{{ route('reports') }}" class="nav-link @if(Route::currentRouteName() == "reports") {{ "active" }} @endif">
              <i class="nav-icon fa fa-file-pdf-o" aria-hidden="true"></i>
              <p>
                Reports
              </p>
            </a>       
          </li>


          <li class="nav-header">MY ACCOUNT</li>
          
          <li class="nav-item">
            <a href="{{ route('change-password') }}" class="nav-link @if(Route::currentRouteName() == "change-password") {{ "active" }} @endif">
              <i class="nav-icon far fa-circle text-warning"></i>
              <p>Change Password</p>
            </a>
          </li>
          
          <li class="nav-item">
            <a href="{{ route('login-logs') }}" class="nav-link @if(Route::currentRouteName() == "login-logs") {{ "active" }} @endif">
              <i class="nav-icon far fa-circle text-info"></i>
              <p>Login Logs</p>
            </a>
          </li>
        
          <li class="nav-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="nav-link">
              <i class="nav-icon far fa-circle text-danger"></i>
              <p class="text">Logout</p>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    @yield("content")
  
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2021 {{ $appName }}</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{ asset("$pF/template/plugins/jquery/jquery.min.js") }}"></script>

<!-- jQuery UI 1.11.4 -->
<script src="{{ asset("$pF/template/plugins/jquery-ui/jquery-ui.min.js") }}"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Bootstrap 4 -->
<script src="{{ asset("$pF/template/plugins/bootstrap/js/bootstrap.bundle.min.js") }}"></script>

<!-- ChartJS -->
<script src="{{ asset("$pF/template/plugins/chart.js/Chart.min.js") }}"></script>

<!-- Sparkline -->
<script src="{{ asset("$pF/template/plugins/sparklines/sparkline.js") }}"></script>

<!-- JQVMap -->
<script src="{{ asset("$pF/template/plugins/jqvmap/jquery.vmap.min.js") }}"></script>
<script src="{{ asset("$pF/template/plugins/jqvmap/maps/jquery.vmap.usa.js") }}"></script>

<!-- jQuery Knob Chart -->
<script src="{{ asset("$pF/template/plugins/jquery-knob/jquery.knob.min.js") }}"></script>

<!-- daterangepicker -->
<script src="{{ asset("$pF/template/plugins/moment/moment.min.js") }}"></script>
<script src="{{ asset("$pF/template/plugins/daterangepicker/daterangepicker.js") }}"></script>

<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset("$pF/template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js") }}"></script>

<!-- Summernote -->
<script src="{{ asset("$pF/template/plugins/summernote/summernote-bs4.min.js") }}"></script>

<!-- overlayScrollbars -->
<script src="{{ asset("$pF/template/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js") }}"></script>

<!-- AdminLTE App -->
<script src="{{ asset("$pF/template/dist/js/adminlte.js") }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset("$pF/template/dist/js/demo.js") }}"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ asset("$pF/template/dist/js/pages/dashboard.js") }}"></script>
</body>
</html>


@include ("incs.header_styles")