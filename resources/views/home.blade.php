@extends('layouts.adminlte')

@section('pageTitle') {{ "Dashboard" }} @endsection

@section('content')


<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Dashboard</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Dashboard</li>
        </ol>
      </div>
    </div>
  </div>
</div>



<section class="content">
  <div class="container-fluid">
    <!-- Small boxes (Stat box) -->
    <div class="row">

    @if (Auth::User()->level == 1)
    
        <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('services') }}');">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>{{ $totalServices }}</h3>

              <p>Services</p>
            </div>
            <div class="icon">
              <i class="fa fa-cubes"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('agents') }}');">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>{{ $totalAgents }}</h3>

              <p>Agents</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>

    @endif

      <!-- ./col -->
      @if (Auth::User()->level == 1)
        <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('admin-bookings') }}');">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner" style="color: white;">
              <h3>{{ $totalBookings }}</h3>

              <p>Bookings</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
      @else
        <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('bookings') }}');">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner" style="color: white;">
              <h3>{{ $totalBookings }}</h3>

              <p>Bookings</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
      @endif

      <!-- ./col -->
      <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('reports') }}');">
        <!-- small box -->
        <div class="small-box bg-danger">
          <div class="inner">
            <h3>-</h3>

            <p>Reports</p>
          </div>
          <div class="icon">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->


      @if (Auth::User()->level == 2)


        <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('login-logs') }}');">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>0</h3>

              <p>Login Logs</p>
            </div>
            <div class="icon">
              <i class="fa fa-cubes"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-6" onclick="window.location.assign('{{ route('agents') }}');">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>-</h3>

              <p>Change My Password</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>

      @endif

    </div>
    <!-- /.row -->
    <!-- Main row -->
    <div class="row">
      

      <h3>Recent Activities</h3>

      <table class="table table-bordered">
        <tr>
          <td>ID</td>
          <td>Activity Title</td>
          <td>Activity Info</td>
          <td>Created</td>
        </tr>

        @php $id = 1; @endphp
        @foreach($recentActivities as $recentActivity)

          <tr>
            <td>{{ $id++ }}</td>
            <td>{{ $recentActivity->activityTitle }}</td>
            <td>{{ $recentActivity->activityInfo }}</td>
            <td>{{ $recentActivity->created_at->diffForHumans() }}</td>
          </tr>

        @endforeach
      </table>


    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
</section>

@endsection
