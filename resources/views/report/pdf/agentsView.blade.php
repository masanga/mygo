@include ("incs.bts")


<div class="container-fluid">
		
	<div class="row">
		<div class="col-md-4">
			<h2>Booking Report</h2>
		</div>
		<br>
		{{-- <br>
		<div class="col-md-4 col-md-offset-4" style="text-align: right;">
			<a href="{{ route('download') }}" style="color: white;" class="btn btn-success" >Download Pdf</a>
		</div> --}}

		{{-- <div class="col-md-4 col-md-offset-4">
			<a href="" class="btn btn-success" style="color: white;">Export to PDF</a>
		</div> --}}
		</div>
	</div>

	<hr/>

		<table class="table table-bordered table-striped">
			<tr style="text-align: center;">
				<td>No </td>
				<td>Service</td>
				<td>Date</td>
				<td>Total Price</td>
				<td>Name</td>
				<td>Phone Number</td>
				<td>Rest Amount </td>
				<td>Notes</td>
			</tr>

			@php $id = 1;
			 use App\Service; 
			 @endphp
			@foreach($serviceReports as $service)
				<tr style="text-align: center;">
					<td>{{ $id++ }}</td>
					<td>{{ service::find($service->serviceSelectedId)->serviceTitle }}</td>
					<td>{{ $service->serviceDateBooking }}</td>
					<td>{{ $service->totalPrice }}{{$service->totalPriceCurrency}}</td>
					<td>{{ $service->nameOfBooking }}</td>
					<td>{{ $service->phoneOfBooking }}</td>
					<td>{{ $service->restPrice }}</td>
					<td>{{ $service->notes }}</td>
				</tr>
			@endforeach

		</table>
</div>