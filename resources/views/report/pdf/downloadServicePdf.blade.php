@include ("incs.bts")


<div class="container-fluid">
		
	<div class="row">
		<div class="col-md-4">
			<h2>Booking Report By Service</h2>
		</div>
	</div>

	<hr/>

		<table class="table table-bordered table-striped">
			<tr style="text-align: center;">
				<td>No </td>
				<td>Service</td>
				<td>Date</td>
				<td>Total Price 1 & 2</td>
				<td>Name</td>
				<td>Phone Number</td>
				<td>Rest Amount </td>
				<td>Notes</td>
			</tr>

			@php $id = 1; @endphp
			@foreach($serviceReports as $service)
				<tr style="text-align: center;">
					<td>{{ $id++ }}</td>
					<td>{{ $service->serviceSelectedId }}</td>
					<td>{{ $service->serviceDateBooking }}</td>
					<td>{{ ($service->numberOfPrice1)+($service->numberOfPrice2) }}</td>
					<td>{{ $service->nameOfBooking }}</td>
					<td>{{ $service->phoneOfBooking }}</td>
					<td>{{ $service->restPrice }}</td>
					<td>{{ $service->notes }}</td>
				</tr>
			@endforeach

		</table>
</div>