@extends('layouts.adminlte')

@section('pageTitle') {{ "Generate Excel Report" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Generate Excel Report</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active"><a href="{{ route('reports') }}">Reports</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

<div class="container">

  <form action="{{ route('servicesReportView') }}" method="GET" target="_blank">

    <table class="table table-bordered">
      <tr>
        <td>Generate Report By</td>
        <td>
          <select class="form-control" name="excel" id="excelFormat" class="form-control" onclick="updateInteractiveFields();">
              <option value="all">All</option>
              <option value="agents">Agent</option>
              <option value="service">Service</option>
              <option value="date">Date</option>         
          </select>
        </td>
      </tr>

      <tbody id="excelFormatData"></tbody>
       <div id="datepicker"></div>

      <tr>
        <td></td>
        <td><input type="submit" class="btn btn-primary form-control" value="Generate Report"></td>
        @csrf
      </tr>
    </table>

  </form>

</div>

@endsection


<script type="text/javascript">
  
  //Updating Interactive Fields
  function updateInteractiveFields() {

    var reportType = $("#excelFormat option:selected").val();

    //Clear HTML Table
    $("#excelFormatData").html("");


      if (reportType == 'agents') {
        

        // Month
        $("#excelFormatData").append(`
          <tr>
            <td>Agent Name</td>
            <td>
            <select name="agents_excel"  class="form-control">
            @foreach ($agents as $agent)
        <option value="{{ $agent->id }}">{{ $agent->firstName }} &nbsp; {{ $agent->lastName }}</option>
            @endforeach
            </select>
            </td>
          </tr>`);

        


      }

      if (reportType == 'service') {
      

      // Month
      $("#excelFormatData").append(`
        <tr>
          <td>Service Title</td>
          <td>
          <select name="service_excel"  class="form-control">
            @foreach($services as $service)
              <option value="{{ $service->id }}">{{ $service->serviceTitle }}</option>
            @endforeach
          </select>
          </td>
        </tr>`);

      


    }

    if (reportType == 'date') {

      // Month
      $("#excelFormatData").append(`

        <tr>
           <td>Select Date</td>
          <td>
           <input  name="date_excel" type="date"  class="form-control">
          </td>
        </tr>`);

      
        var serviceDateBooking = $("#datepicker2").val();

    }

    

  
    
  }

</script>