@extends('layouts.adminlte')

@section('pageTitle') {{ "Generate Csv Report" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Generate CSV Report</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active"><a href="{{ route('reports') }}">Reports</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

<div class="container">

  <form action="{{ route('generateBookingsReportView') }}" method="GET" target="_blank">

    <table class="table table-bordered">
      <tr>
        <td>Generate Report By</td>
        <td>
          <select class="form-control" name="csv" id="csvFormat" class="form-control" onclick="updateInteractiveFields();">
              <option value="all">All</option>
              <option value="agents">Agent</option>
              <option value="service">Service</option>
              <option value="date">Date</option>         
          </select>
        </td>
      </tr>

      <tbody id="csvFormatData"></tbody>
       <div id="datepicker"></div>

      <tr>
        <td></td>
        <td><input type="submit" class="btn btn-primary form-control" value="Generate Report"></td>
        @csrf
      </tr>
    </table>

  </form>

</div>

@endsection


<script type="text/javascript">
  
  //Updating Interactive Fields
  function updateInteractiveFields() {

    var reportType = $("#csvFormat option:selected").val();

    //Clear HTML Table
    $("#csvFormatData").html("");


      if (reportType == 'agents') {
        

        // Month
        $("#csvFormatData").append(`
          <tr>
            <td>Agent Name</td>
            <td>
            <select name="agents_csv"  class="form-control">
            @foreach ($agents as $agent)
        <option value="{{ $agent->id }}">{{ $agent->firstName }} &nbsp; {{ $agent->lastName }}</option>
            @endforeach
            </select>
            </td>
          </tr>`);

        


      }

      if (reportType == 'service') {
      

      // Month
      $("#csvFormatData").append(`
        <tr>
          <td>Service Title</td>
          <td>
          <select name="service_csv"  class="form-control">
            @foreach($services as $service)
              <option value="{{ $service->id }}">{{ $service->serviceTitle }}</option>
            @endforeach
          </select>
          </td>
        </tr>`);

      


    }

    if (reportType == 'date') {

      // Month
      $("#csvFormatData").append(`

        $('#datepicker').datepicker({dateFormat: 'dd/mm/yy', altField: '#datepicker2'});  

        <tr>
           <td>Select Date</td>
          <td>
           <input  name="date_csv" type="date" class="form-control">
          </td>
        </tr>`);

      

       var serviceDateBooking = $("#datepicker2").val();

    }

        
  }

</script>