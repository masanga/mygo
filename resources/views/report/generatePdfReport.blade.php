@extends('layouts.adminlte')

@section('pageTitle') {{ "Generate Pdf Report" }} @endsection

@section('content')


<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Generate PDF Report</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active"><a href="{{ route('reports') }}">Reports</a></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

<div class="container">

  <form action="{{ route('generateAgentsReportView') }}" method="GET" target="_blank">
    <table class="table table-bordered">
      <tr>
        <td>Generate Report By</td>
        <td>
          <select class="form-control" name="pdf" id="pdfFormat" class="form-control" onclick="updateInteractiveFields();">
              <option value="all">All</option>
              <option value="agents">Agent</option>
              <option value="service">Service</option>
              <option value="date">Date</option>         
          </select>
        </td>
      </tr>

      <tbody id="pdfFormatData"></tbody>


      <tr>
        <td></td>
        <td><input type="submit" class="btn btn-primary form-control" value="Generate Report"></td>
        @csrf
      </tr>
    </table>

  </form>

</div>

@endsection


<script type="text/javascript">

  //Updating Interactive Fields
  function updateInteractiveFields() {

    var reportType = $("#pdfFormat option:selected").val();

    //Clear HTML Table
    $("#pdfFormatData").html("");


      if (reportType == 'agents') {
        

        // Month
        $("#pdfFormatData").append(`
          <tr>
            <td>Agent Name</td>
            <td>
            <select name="agents_pdf"  class="form-control">
            @foreach ($agents as $agent)
        <option value="{{ $agent->id }}">{{ $agent->firstName }} &nbsp; {{ $agent->lastName }}</option>
            @endforeach
            </select>
            </td>
          </tr>`);

        


      }

      if (reportType == 'service') {
      

      // Month
      $("#pdfFormatData").append(`
        <tr>
          <td>Service Title</td>
          <td>
          <select name="service_pdf"  class="form-control">
            @foreach($services as $service)
              <option value="{{ $service->id }}">{{ $service->serviceTitle }}</option>
            @endforeach
          </select>
          </td>
        </tr>`);

      


    }

    if (reportType == 'date') {

      // Month
      $("#pdfFormatData").append(`
        <tr>
           <td>Select Date</td>
          <td>
           <input  name="date_pdf" type="date"  class="form-control">
          </td>
        </tr>`);

    }

        
  }

</script>