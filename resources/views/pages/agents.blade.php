@extends('layouts.adminlte')

@section('pageTitle') {{ "Agents" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Agents ({{ $agentsTotal }})&nbsp; <span class="fa fa-plus" data-toggle="modal" data-target="#addNewAgentModal"></span></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Agents</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>


<!-- Add New Agent Modal -->
<div class="modal fade" id="addNewAgentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add New Agent</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <form method="POST" action="{{ route('postNewAgent') }}">
          <table>
            <tr>
              <td>First Name <span class="red">*</span></td>
              <td>
                <input type="text" name="firstName" class="form-control" required="true">
              </td>
            </tr>

            <tr>
              <td>Last Name <span class="red">*</span></td>
              <td><input type="text" name="lastName" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>Email <span class="red">*</span></td>
              <td><input type="email" name="email" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>Phone Number</td>
              <td><input type="text" name="phoneNumber" class="form-control"></td>
            </tr>

            <tr>
              <td>Agent Class</td>
              <td>
                <select name="agentClass" class="form-control" required="true">
                  <option value="BRONZE">Bronze</option>
                  <option value="SILVER">Silver</option>
                  <option value="GOLD">Gold</option>
                  <option value="DIAMOND">Diamond</option>
                </select>
              </td>
            </tr>

            <tr>
              <td>Password <span class="red">*</span></td>
              <td><input type="password" name="password_confirmation" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>Verify Password <span class="red">*</span></td>
              <td><input type="password" name="password" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td><input type="hidden" name="_token" value="{{ Session::token() }}"></td>
              <td><input type="submit" class="btn btn-primary form-control"></td>
            </tr>
          </table>
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid">
  

<table class="table table-bordered">
  <tr>
    <td>ID</td>
    <td>First Name</td>
    <td>Last Name</td>
    <td>Email</td>
    <td>Phone Number</td>
    <td class="center">Actions</td>
  </tr>


  @php $id = 1; @endphp
  @foreach($agents as $agent)

    <tr>
      <td>{{ $id++ }}</td>
      <td>{{ $agent->firstName }}</td>
      <td>{{ $agent->lastName }}</td>
      <td>{{ $agent->email }}</td>
      <td>{{ $agent->phoneNumber }}</td>
      <td>
        
        <div class="container-fluid">
          
            <div class="row">
              
                <div class="col-md-4">
                  <img src="{{ asset("$pF/pics/service_add.png") }}" style="width: 40px; height: 40px;" data-toggle="modal" data-target="#agentsChangeServicesModal" onclick="fillServicesModalData('{{ $agent->id }}');">
                </div>

                <div class="col-md-4">
                  <img src="{{ asset("$pF/pics/edit_icon.png") }}" style="width: 40px; height: 40px;" data-toggle="modal" data-target="#editAgentModal" onclick="editAgentJSTrigger('{{ $agent->id }}', '{{ $agent->firstName }}', '{{ $agent->lastName }}', '{{ $agent->email }}', '{{ $agent->phoneNumber }}');">
                </div>

                <div class="col-md-4">
                  <img src="{{ asset("$pF/pics/delete_icon.png") }}" style="width: 40px; height: 40px;" onclick="deleteAgentInfo('{{ $agent->id }}');">
                </div>

            </div>

        </div>

      </td>
    </tr>

  @endforeach
</table>

<div class="pagination">
  {{ $agents->links() }}
</div>

</div>


<!-- Agent Services Modal -->
<div class="modal fade" id="agentsChangeServicesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Agents Services (<span id="agentServiceModalBody"></span> Agent)</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
      <table class="table table-bordered">
        <tr>
          <td>ID</td>
          <td>Service Title</td>
          <td>Price Amount</td>
          <td>Action</td>
        </tr>

        <tbody id="agentServicesTbody"></tbody>
      </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Edit Agent Modal -->
<div class="modal fade" id="editAgentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Agent</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('postUpdateAgentDetails') }}">
            
          <table>
              <tr>
                <td>First Name <span class="red">*</span></td>
                <td><input type="text" name="agentFirstName" id="agentFirstName" class="form-control"></td>
              </tr>

              <tr>
                <td>Last Name <span class="red">*</span></td>
                <td><input type="text" name="agentLastName" id="agentLastName" class="form-control"></td>
              </tr>

              <tr>
                <td>Email <span class="red">*</span></td>
                <td><input type="text" name="agentEmail" id="agentEmail" class="form-control"></td>
              </tr>


              <tr>
                <td>Phone Number</td>
                <td><input type="text" name="agentPhoneNumber" id="agentPhoneNumber" class="form-control"></td>
              </tr>

              <tr>
                <td>
                  <input type="hidden" name="agentIdOnEdit" id="agentIdOnEdit">
                  <input type="hidden" name="_token" value="{{ Session::token() }}">
                </td>
                <td><input type="submit" value="Update Agent Data" class="form-control"></td>
              </tr>            
          </table>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

  function fillServicesModalData(agentId) {

    $.get("{{ goPL() }}/admin/agent/services/list/"+agentId, function (data) {

      var jsonData = JSON.parse(JSON.stringify(data));

      var data = jsonData.data;

      $("#agentServicesTbody").html("");      


      $("#agentServiceModalBody").append(data[0].agentClass);

      var id = 1;
      data.forEach(function (agentS) {

        var action_button = "";

        if (agentS.serviceAdded == "FALSE") {
          action_button = "<button class='btn btn-primary form-control' onclick='addServiceAgent(\""+agentId+"\", \""+agentS.serviceId+"\" );'>Add Service</button>";
        }else {
          action_button = "<button class='btn btn-danger form-control' onclick='removeServiceAgent(\""+agentId+"\", \""+agentS.serviceId+"\" );'>Remove Service</button>";
        }

        $("#agentServicesTbody").append(`

          <tr>
            <td>`+id+`</td>
            <td>`+agentS.serviceTitle+`</td>
            <td>Price 1 : <strong>`+agentS.priceAmount1+ ' ' + agentS.priceCurrency1 +`</strong></br>
              Price 2 : <strong>`+agentS.priceAmount2+ ' ' + agentS.priceCurrency2 +`</strong>
            </td>
            <td id='action_button_`+agentS.serviceId+`'>`+action_button+`</td>
          </tr>

          `);

        id++;


      });


    });

  }


  function addServiceAgent(agentId, serviceId) {
      $.get('{{ goPL() }}/add/service/to/agent/'+agentId+"/"+serviceId, function (data) {

        var jsonData = JSON.parse(JSON.stringify(data));

        if (jsonData.status == "SUCCESS") {

          Swal.fire(
            'Success',
            'Service Was Added Successfully to Agent',
            'success'
          ).then(function () {
            //--
            action_button = "<button class='btn btn-danger form-control' onclick='removeServiceAgent(\""+agentId+"\", \""+serviceId+"\" );'>Remove Service</button>";
            $("#action_button_"+serviceId).html(action_button);
          });

        }

      });
  }

  function removeServiceAgent(agentId, serviceId) {
      $.get('{{ goPL() }}/remove/service/to/agent/'+agentId+"/"+serviceId, function (data) {

        var jsonData = JSON.parse(JSON.stringify(data));

        if (jsonData.status == "SUCCESS") {

          Swal.fire(
            'Success',
            'Service Was Removed Successfully from Agent',
            'success'
          ).then(function () {
            //--
            action_button = "<button class='btn btn-primary form-control' onclick='addServiceAgent(\""+agentId+"\", \""+serviceId+"\" );'>Add Service</button>";
            $("#action_button_"+serviceId).html(action_button);
          });

        }

      });
  }

  function editAgentJSTrigger(agentId, agentFirstName, agentLastName, agentEmail, agentPhoneNumber) {

    $("#agentIdOnEdit").val(agentId);
    $("#agentFirstName").val(agentFirstName);
    $("#agentPhoneNumber").val(agentPhoneNumber);
    $("#agentLastName").val(agentLastName);
    $("#agentEmail").val(agentEmail);

  }

  function deleteAgentInfo(agentId)
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete Agent!'
    }).then((result) => {
      if (result.value) {

        $.get('/admin/delete/agent/'+agentId, function (data) {

          var jsonData = JSON.parse(JSON.stringify(data));

          if (jsonData.status == "SUCCESS") {

            Swal.fire(
              'Agent Deleted!',
              'Agent Was Deleted Successfully',
              'success'
            ).then(function () {
              window.location.reload();
            });

          }else {
            Swal.fire(
              'Error!',
              'Failed to Delete Agent. Please Try Again Later',
              'error'
            );         
          }

        });

      }
    })
  }
</script>

@endsection
