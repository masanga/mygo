@extends('layouts.adminlte')

@section('pageTitle') {{ "Bookings" }} @endsection

@include ("incs.newBookingModal")

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Bookings <span class="fa fa-plus" data-toggle="modal" data-target="#newBookingModal"></span></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Bookings</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

@endsection
