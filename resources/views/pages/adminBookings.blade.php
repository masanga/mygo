@extends('layouts.adminlte')

@section('pageTitle') {{ "Admin Bookings" }} @endsection

@include ("incs.newBookingModal")

@php

use App\User;
use App\Service;

@endphp

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Bookings <span class="fa fa-plus" data-toggle="modal" data-target="#newBookingModal"></span></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Bookings</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>

<div class="container-fluid">

  <table class="table table-bordered">
    <tr>
      <td>ID</td>
      <td>Agent</td>
      <td>Sound Record</td>
      <td>Service</td>
      <td>Date</td>
      <td>No of Price 1</td>
      <td>No of Price 2</td>
      
      <td>Name</td>
      <td>Phone Number</td>

      <td>Total Price</td>
      <td>Deposit Price</td>
      <td>Rest Amount</td>
      <td>Notes</td>
    </tr>


    @php $id = 1; @endphp

    @foreach($bookings as $booking)

      <tr>
        <td>{{ $id++ }}</td>
        <td>{{ User::find($booking->agentId)->firstName }} {{ User::find($booking->agentId)->lastName }}</td>
        <td><audio id="adioPlay" src="{{ goPL() }}/storage/app/audios/file_{{ $booking->id }}.ogg" controls></audio> </td>
        <td>{{ Service::find($booking->serviceSelectedId)->serviceTitle }}</td>
        <td>{{ $booking->serviceDateBooking }}</td>
        <td>{{ $booking->numberOfPrice1 }}</td>
        <td>{{ $booking->numberOfPrice2 }}</td>

        <td>{{ $booking->nameOfBooking }}</td>
        <td>{{ $booking->phoneOfBooking }}</td>

        <td>{{ $booking->totalPrice }} {{ $booking->totalPriceCurrency }}</td>
        <td>{{ $booking->depositPaymentPrice }} {{ $booking->depositPaymentCurrency }}</td>
        <td>{{ $booking->restPrice }} {{ $booking->restCurrency }}</td>
        <td>{{ $booking->notes }}</td>
      </tr>

    @endforeach
  </table>

  <div class="pagination">
    {{ $bookings->links() }}
  </div>

</div>

@endsection

{{-- <script type="text/javascript"> 
    window.onload = function() {
        var audioPlayer = document.getElementById("adioPlay");
        audioPlayer.load();
        audioPlayer.play();
    };
    </script> 
 --}}