@extends('layouts.adminlte')

@section('pageTitle') {{ "Reports" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Booking Reports</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Reports</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>


<div class="container">
  
  <div class="row">
    <div class="col-sm-6 col-md-4 center" onclick="window.location.assign('{{ route('pdf-reports') }}');">
      <div class="thumbnail" style="background: white; border: 2px solid #eee;">
        <img src="{{ asset("$pF/pics/pdf1.png") }}" alt="Agents Report" style="width: 250px; height: 250px;">
        <div class="caption">
          <h3>Pdf Format Reports</h3>
          <hr/>
          <p><a href="{{ route('pdf-reports') }}" class="btn btn-primary" role="button">Generate PDF Report</a></p>
        </div>
      </div>
    </div>

    <div class="col-sm-6 col-md-4 center" onclick="window.location.assign('{{ route('excel-reports') }}');">
      <div class="thumbnail" style="background: white; border: 2px solid #eee;">
        <img src="{{ asset("$pF/pics/excel.png") }}" alt="..." style="width: 300px; height: 250px;">
        <div class="caption">
          <h3>Excel Format Reports</h3>
          <hr/>
          <p><a href="{{ route('excel-reports') }}" class="btn btn-primary" role="button">Generate Excel Report</a></p>
        </div>
      </div>
    </div>

    <div class="col-sm-6 col-md-4 center" onclick="window.location.assign('{{ route('csv-reports') }}');">
      <div class="thumbnail" style="background: white; border: 2px solid #eee;">
        <img src="{{ asset("$pF/pics/bookings_report.jpg") }}" alt="..." style="width: 250px; height: 250px;">
        <div class="caption">
          <h3>CSV Format Reports</h3>
          <hr/>
          <p><a href="{{ route('csv-reports') }}" class="btn btn-primary" role="button">Generate CSV Report</a></p>
        </div>
      </div>
    </div>

  </div>

</div>

@endsection

{{-- 
@extends('layouts.adminlte')

@section('pageTitle') {{ "Reports" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Booking Reports</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Reports</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>


<div class="container">
  
  <div class="row">
    <div class="col-sm-6 col-md-4 center" onclick="window.location.assign('{{ route('agents-reports') }}');">
      <div class="thumbnail" style="background: white; border: 2px solid #eee;">
        <img src="{{ asset("$pF/pics/agents_report.png") }}" alt="Agents Report" style="width: 250px; height: 250px;">
        <div class="caption">
          <h3>Report by Agent of Booking</h3>
          <hr/>
          <p><a href="{{ route('agents-reports') }}" class="btn btn-primary" role="button">Generate Report</a></p>
        </div>
      </div>
    </div>

    <div class="col-sm-6 col-md-4 center" onclick="window.location.assign('{{ route('services-reports') }}');">
      <div class="thumbnail" style="background: white; border: 2px solid #eee;">
        <img src="{{ asset("$pF/pics/services_report.png") }}" alt="..." style="width: 300px; height: 250px;">
        <div class="caption">
          <h3>Report by Service </h3>
          <hr/>
          <p><a href="{{ route('services-reports') }}" class="btn btn-primary" role="button">Generate Report</a></p>
        </div>
      </div>
    </div>

    <div class="col-sm-6 col-md-4 center" onclick="window.location.assign('{{ route('bookings-reports') }}');">
      <div class="thumbnail" style="background: white; border: 2px solid #eee;">
        <img src="{{ asset("$pF/pics/bookings_report.jpg") }}" alt="..." style="width: 250px; height: 250px;">
        <div class="caption">
          <h3> Report by Date </h3>
          <hr/>
          <p><a href="{{ route('bookings-reports') }}" class="btn btn-primary" role="button">Generate Report</a></p>
        </div>
      </div>
    </div>

  </div>

</div>

@endsection --}}
