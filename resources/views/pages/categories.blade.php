@extends('layouts.adminlte')

@section('pageTitle') {{ "Service Categories" }} @endsection

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Service Categories <span class="fa fa-plus" data-toggle="modal" data-target="#addNewCategoryModal"></span></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Service Categories</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>


<!-- Add New Category Modal -->
<div class="modal fade" id="addNewCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add New Service Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{ route('postNewCategory') }}">
          <table>
            <tr>
              <td>Category Title <span class="red">*</span></td>
              <td><input type="text" name="categoryTitle" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>Category Info</td>
              <td><input type="text" name="categoryInfo" class="form-control"></td>
            </tr>

            <tr>
              <td><input type="hidden" name="_token" value="{{ Session::token() }}"></td>
              <td><input type="submit" value="Submit" class="btn btn-primary form-control"></td>
            </tr>
          </table>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid">
  
<table class="table table-bordered">
  <tr>
    <td>ID</td>
    <td>Category Title</td>
    <td>Category Info</td>
    <td>Actions</td>
  </tr>

  @php $id = 1; @endphp
  @foreach($categories as $cat)
    <tr>
      <td>{{ $id++ }}</td>
      <td>{{ $cat->categoryTitle }}</td>
      <td>{{ $cat->categoryInfo }}</td>
      <td>
        <div class="container-fluid">
          
            <div class="row">
              
                <div class="col-md-4 offset-3">
                  <img src="{{ asset("$pF/pics/edit_icon.png") }}" style="width: 40px; height: 40px;" data-toggle="modal" data-target="#editCategoryModal" onclick="editCategory('{{ $cat->id }}', '{{ $cat->categoryTitle }}', '{{ $cat->categoryInfo }}');">
                </div>

                <div class="col-md-4">
                  <img src="{{ asset("$pF/pics/delete_icon.png") }}" style="width: 40px; height: 40px;" onclick="deleteCategory('{{ $cat->id }}');">
                </div>

            </div>

        </div>
      </td>
    </tr>
  @endforeach
</table>

<div class="pagination">
  {{ $categories->links() }}
</div>

</div>


<!-- Edit Category Modal -->
<div class="modal fade" id="editCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <form method="POST" action="{{ route('postUpdateCategory') }}">
          <table>
            <tr>
              <td>Category Title</td>
              <td><input type="text" name="categoryTitle" id="categoryTitle" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>Category Info</td>
              <td><input type="text" name="categoryInfo" id="categoryInfo" class="form-control" required="true"></td>
            </tr>

            <tr>
              <td>
                <input type="hidden" name="categoryId" id="categoryId">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
              </td>
              <td><input type="submit" value="Submit" class="btn btn-primary form-control"></td>
            </tr>
          </table>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  
  function editCategory(categoryId, categoryTitle, categoryInfo) {
    $("#categoryId").val(categoryId);
    $("#categoryTitle").val(categoryTitle);
    $("#categoryInfo").val(categoryInfo);
  }

  function deleteCategory(categoryId)
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete Category!'
    }).then((result) => {
      if (result.value) {

        $.get('/admin/delete/category/'+categoryId, function (data) {

          var jsonData = JSON.parse(JSON.stringify(data));

          if (jsonData.status == "SUCCESS") {

            Swal.fire(
              'Category Deleted!',
              'Category Was Deleted Successfully',
              'success'
            ).then(function () {
              window.location.reload();
            });

          }else {
            Swal.fire(
              'Error!',
              'Failed to Delete Category. Please Try Again Later',
              'error'
            );         
          }

        });

      }
    })
  }

</script>

@endsection
