@extends('layouts.adminlte')

@section('pageTitle') {{ "Services" }} @endsection

@php

use App\Category;

@endphp

@section('content')

<br/>

<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Services ({{ $servicesTotal }})&nbsp;<span class="fa fa-plus" data-toggle="modal" data-target="#addNewServiceModal"></span></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ $appName }}</a></li>
          <li class="breadcrumb-item active">Services</li>
        </ol>
      </div>
    </div>
  </div>
</div>

<hr/>


<!-- Add New Tour Modal -->
<div class="modal fade" id="addNewServiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add New Service</h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
      <div class="modal-body">
        
      <form method="POST" enctype="multipart/form-data" action="{{ route('postNewService') }}">
        
      <table>

        <tr>
          <td>Service Category <span class="red">*</span></td>
          <td  colspan="2">
              <select name="serviceCategory" class="form-control">
                @foreach($serviceCategories as $cat)
                  <option value="{{ $cat->id }}">{{ $cat->categoryTitle }}</option>
                @endforeach
              </select>
          </td>
        </tr>
        
        <tr>
          <td>Service Title <span class="red">*</span></td>
          <td  colspan="2"><input type="text" name="serviceTitle" class="form-control" required="true"></td>
        </tr>

        <tr>
          <td>Service Info</td>
          <td colspan="2">
            <textarea name="serviceInfo" class="form-control"></textarea>
          </td>
        </tr>

        <tr>
          <td colspan="3"><strong>Bronze</strong> <div style="float: right;" class="bronze_tbody_action" onclick="toggle_bronze();"> <span class="fa fa-arrow-up"></span> </div> </td>
        </tr>

        <tbody class="bronze_tbody">
          
          <tr>
            <td>Price <span class="red">*</span></td>
            <td>Currency <span class="red">*</span></td>
            <td>Description</td>
          </tr>

          <tr>
            <td><input type="number" name="bronze_price_a_1" placeholder="1st Price" class="form-control"></td>
            <td>
                <select name="bronze_currency_a_1" class="form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="bronze_description_a_1" placeholder="1st Description" class="form-control"></td>
          </tr>

          <tr>
            <td><input type="number" name="bronze_price_a_2" placeholder="2nd Price" class="form-control"></td>
            <td>
                <select name="bronze_currency_a_2" class="form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="bronze_description_a_2" placeholder="2nd Description" class="form-control"></td>
          </tr>

        </tbody>


        <tr>
          <td colspan="3"><strong>Silver</strong> <div style="float: right;" class="silver_tbody_action" onclick="toggle_silver();"> <span class="fa fa-arrow-down"></span> </div> </td>
        </tr>

        <tbody class="silver_tbody">
          
          <tr>
            <td>Price</td>
            <td>Currency</td>
            <td>Description</td>
          </tr>

          <tr>
            <td><input type="number" name="silver_price_a_1" placeholder="1st Price" class="form-control"></td>
            <td>
                <select name="silver_currency_a_1" class="form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="silver_description_a_1" placeholder="2nd Price" class="form-control"></td>
          </tr>

          <tr>
            <td><input type="number" name="silver_price_a_2" placeholder="2nd Price" class="form-control"></td>
            <td>
                <select name="silver_currency_a_2" class="form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="silver_description_a_2" placeholder="2nd Description"  class="form-control"></td>
          </tr>

        </tbody>


        <tr>
          <td colspan="3"><strong>Gold</strong> <div style="float: right;" class="gold_tbody_action" onclick="toggle_gold();"> <span class="fa fa-arrow-down"></span> </div> </td>
        </tr>

        <tbody class="gold_tbody">
          
          <tr>
            <td>Price</td>
            <td>Currency</td>
            <td>Description</td>
          </tr>

          <tr>
            <td><input type="number" name="gold_price_a_1" placeholder="1st Price" class="form-control"></td>
            <td>
                <select name="gold_currency_a_1" class="form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="gold_description_a_1" placeholder="1st Description" class="form-control"></td>
          </tr>

          <tr>
            <td><input type="number" name="gold_price_a_2" placeholder="2nd Price" class="form-control"></td>
            <td>
                <select name="gold_currency_a_2" class="form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="gold_description_a_2" placeholder="2nd Description" class="form-control"></td>
          </tr>

        </tbody>



        <tr>
          <td colspan="3"><strong>Diamond</strong> <div style="float: right;" class="diamond_tbody_action" onclick="toggle_diamond();"> <span class="fa fa-arrow-down"></span> </div> </td>
        </tr>

        <tbody class="diamond_tbody">
          
          <tr>
            <td>Price</td>
            <td>Currency</td>
            <td>Description</td>
          </tr>

          <tr>
            <td><input type="number" name="diamond_price_a_1" placeholder="1st Price" class="diamond_price_a_1 form-control"></td>
            <td>
                <select name="diamond_currency_a_1" class="diamond_currency_a_1 form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="diamond_description_a_1" placeholder="1st Description" class="diamond_description_a_1 form-control"></td>
          </tr>

          <tr>
            <td><input type="number" name="diamond_price_a_2" placeholder="2st Price" class="diamond_price_a_2 form-control"></td>
            <td>
                <select name="diamond_currency_a_2" class="diamond_currency_a_2 form-control">
                  <option>TL</option>
                  <option>€</option>
                  <option>$</option>
                  <option>£</option>
                </select>
            </td>
            <td><input type="text" name="diamond_description_a_2" placeholder="2nd Description" class="form-control"></td>
          </tr>

        </tbody>

        <tr>
          <td>Service Image(1) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage1" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(2) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage2" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(3) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage3" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(4) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage4" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(5) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage5" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(6) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage6" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(7) <span class="red"></span></td>
          <td><input type="file" name="serviceImage7" class="form-control"></td>
        </tr>

        <tr>
          <td>Service Image(8) <span class="red"></span></td>
          <td  colspan="2"><input type="file" name="serviceImage8" class="form-control"></td>
        </tr>

        <tr>
          <td><input type="hidden" name="_token" value="{{ Session::token() }}"></td>
          <td  colspan="2"><input type="submit" class="btn btn-primary form-control"></td>
        </tr>
      </table>

      </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid">
  
<table class="table table-bordered">
  <tr>
    <td>ID</td>
    <td>Service Title</td>
    <td>Service Info</td>
    <td>Service Image</td>
    <td>Service Category</td>
    <td class="center">Actions</td>
  </tr>

  @php $id = 1; @endphp
  @foreach($servicesList as $service)
    <tr>
      <td>{{ $id++ }}</td>
      <td>{{ $service->serviceTitle }}</td>
      <td>{{ $service->serviceInfo }}</td>
      <td>
        <button class="btn btn-primary" data-toggle="modal" data-target="#serviceImagesModal" onclick="fillImageOnModal('{{ $service->id }}', '{{ $service->serviceImage1 }}', '{{ $service->serviceImage2 }}', '{{ $service->serviceImage3 }}', '{{ $service->serviceImage4 }}', '{{ $service->serviceImage5 }}', '{{ $service->serviceImage6 }}', '{{ $service->serviceImage7 }}', '{{ $service->serviceImage8 }}');">View Images</button>
      </td>
      <td>{{ Category::find($service->serviceCategory)->categoryTitle }}</td>
      <td>
        <div class="row">

            <div class="col-md-3 offset-2">
              <img src="{{ asset("$pF/pics/view_icon.png") }}" style="width: 40px; height: 40px;"  data-toggle="modal" data-target="#viewServiceDataModal" onclick="viewServiceInfo('{{ $service->id }}', '{{ $service->status }}', '{{ $service->serviceCategory }}', '{{ Category::find($service->serviceCategory)->categoryTitle }}', '{{ $service->serviceTitle }}', '{{ $service->serviceInfo }}', '{{ $service->bronze_price_a_1 }}', '{{ $service->bronze_currency_a_1 }}', '{{ $service->bronze_description_a_1 }}', '{{ $service->bronze_price_a_2 }}', '{{ $service->bronze_currency_a_2 }}', '{{ $service->bronze_description_a_2 }}', '{{ $service->silver_price_a_1 }}', '{{ $service->silver_currency_a_1 }}', '{{ $service->silver_description_a_1 }}', '{{ $service->silver_price_a_2 }}', '{{ $service->silver_currency_a_2 }}', '{{ $service->silver_description_a_2 }}', '{{ $service->gold_price_a_1 }}', '{{ $service->gold_currency_a_1 }}', '{{ $service->gold_description_a_1 }}', '{{ $service->gold_price_a_2 }}', '{{ $service->gold_currency_a_2 }}', '{{ $service->gold_description_a_2 }}', '{{ $service->diamond_price_a_1 }}', '{{ $service->diamond_currency_a_1 }}', '{{ $service->diamond_description_a_1 }}', '{{ $service->diamond_price_a_2 }}', '{{ $service->diamond_currency_a_2 }}', '{{ $service->diamond_description_a_2 }}');">  
            </div>

            <div class="col-md-3">
              <img src="{{ asset("$pF/pics/edit_icon.png") }}" style="width: 40px; height: 40px;" data-toggle="modal" data-target="#editServiceModal" onclick="editServiceInfo('{{ $service->id }}', '{{ $service->status }}', '{{ $service->serviceCategory }}', '{{ Category::find($service->serviceCategory)->categoryTitle }}', '{{ $service->serviceTitle }}', '{{ $service->serviceInfo }}', '{{ $service->bronze_price_a_1 }}', '{{ $service->bronze_currency_a_1 }}', '{{ $service->bronze_description_a_1 }}', '{{ $service->bronze_price_a_2 }}', '{{ $service->bronze_currency_a_2 }}', '{{ $service->bronze_description_a_2 }}', '{{ $service->silver_price_a_1 }}', '{{ $service->silver_currency_a_1 }}', '{{ $service->silver_description_a_1 }}', '{{ $service->silver_price_a_2 }}', '{{ $service->silver_currency_a_2 }}', '{{ $service->silver_description_a_2 }}', '{{ $service->gold_price_a_1 }}', '{{ $service->gold_currency_a_1 }}', '{{ $service->gold_description_a_1 }}', '{{ $service->gold_price_a_2 }}', '{{ $service->gold_currency_a_2 }}', '{{ $service->gold_description_a_2 }}', '{{ $service->diamond_price_a_1 }}', '{{ $service->diamond_currency_a_1 }}', '{{ $service->diamond_description_a_1 }}', '{{ $service->diamond_price_a_2 }}', '{{ $service->diamond_currency_a_2 }}', '{{ $service->diamond_description_a_2 }}');">
            </div>

            <div class="col-md-3">
              <img src="{{ asset("$pF/pics/delete_icon.png") }}" style="width: 40px; height: 40px;" onclick="deleteServiceInfo('{{ $service->id }}');">
            </div>

        </div>
      </td>
    </tr>
  @endforeach

</table>

<div class="pagination">
  {{ $servicesList->links() }}
</div>

</div>

<!-- Edit Service Modal -->
<div class="modal fade" id="editServiceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Edit Service Data</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">

          <form method="POST" enctype="multipart/form-data" action="{{ route('postUpdateServiceData') }}">
            
          <table>

            <tr>
              <td>Service Category <span class="red">*</span></td>
              <td colspan="2">
                  <select name="serviceCategory" id="serviceCategory" class="serviceCategory form-control">
                    @foreach($serviceCategories as $cat)
                      <option value="{{ $cat->id }}">{{ $cat->categoryTitle }}</option>
                    @endforeach
                  </select>
              </td>
            </tr>
            
            <tr>
              <td>Service Title <span class="red">*</span></td>
              <td colspan="2"><input type="text" name="serviceTitle" id="serviceTitle" class="serviceTitle form-control" required="true"></td>
            </tr>

            <tr>
              <td>Service Info</td>
              <td colspan="2">
                <textarea name="serviceInfo" id="serviceInfo" class="serviceInfo form-control"></textarea>
              </td>
            </tr>



            <tr>
              <td colspan="3"><strong>Bronze</strong> <div style="float: right;" class="bronze_tbody_action" onclick="toggle_bronze();"> <span class="fa fa-arrow-up"></span> </div> </td>
            </tr>

            <tbody class="bronze_tbody">
              
              <tr>
                <td>Price <span class="red">*</span></td>
                <td>Currency <span class="red">*</span></td>
                <td>Description</td>
              </tr>

              <tr>
                <td><input type="number" name="bronze_price_a_1" placeholder="1st Price" class="bronze_price_a_1 form-control"></td>
                <td>
                    <select name="bronze_currency_a_1" class="bronze_currency_a_1 form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="bronze_description_a_1" placeholder="1st Description" class="bronze_description_a_1 form-control"></td>
              </tr>

              <tr>
                <td><input type="number" name="bronze_price_a_2" placeholder="2nd Price" class="bronze_price_a_2 form-control"></td>
                <td>
                    <select name="bronze_currency_a_2" class="bronze_currency_a_2 form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="bronze_description_a_2" placeholder="2nd Description" class="bronze_description_a_2 form-control"></td>
              </tr>

            </tbody>


            <tr>
              <td colspan="3"><strong>Silver</strong> <div style="float: right;" class="silver_tbody_action" onclick="toggle_silver();"> <span class="fa fa-arrow-down"></span> </div> </td>
            </tr>

            <tbody class="silver_tbody">
              
              <tr>
                <td>Price</td>
                <td>Currency</td>
                <td>Description</td>
              </tr>

              <tr>
                <td><input type="number" name="silver_price_a_1" placeholder="1st Price" class="silver_price_a_1 form-control"></td>
                <td>
                    <select name="silver_currency_a_1" class="silver_currency_a_1 form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="silver_description_a_1" placeholder="2nd Price" class="silver_description_a_1 form-control"></td>
              </tr>

              <tr>
                <td><input type="number" name="silver_price_a_2" placeholder="2nd Price" class="silver_price_a_2 form-control"></td>
                <td>
                    <select name="silver_currency_a_2" class="silver_currency_a_2 form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="silver_description_a_2" placeholder="2nd Description"  class="silver_description_a_2 form-control"></td>
              </tr>

            </tbody>


            <tr>
              <td colspan="3"><strong>Gold</strong> <div style="float: right;" class="gold_tbody_action" onclick="toggle_gold();"> <span class="fa fa-arrow-down"></span> </div> </td>
            </tr>

            <tbody class="gold_tbody">
              
              <tr>
                <td>Price</td>
                <td>Currency</td>
                <td>Description</td>
              </tr>

              <tr>
                <td><input type="number" name="gold_price_a_1" placeholder="1st Price" class="gold_price_a_1 form-control"></td>
                <td>
                    <select name="gold_currency_a_1" class="gold_currency_a_1 form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="gold_description_a_1" placeholder="1st Description" class="gold_description_a_1 form-control"></td>
              </tr>

              <tr>
                <td><input type="number" name="gold_price_a_2" placeholder="2nd Price" class="gold_price_a_2 form-control"></td>
                <td>
                    <select name="gold_currency_a_2" class="gold_currency_a_2 form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="gold_description_a_2" placeholder="2nd Description" class="gold_description_a_2 form-control"></td>
              </tr>

            </tbody>

            <tr>
              <td colspan="3"><strong>Diamond</strong> <div style="float: right;" class="diamond_tbody_action" onclick="toggle_diamond();"> <span class="fa fa-arrow-down"></span> </div> </td>
            </tr>

            <tbody class="diamond_tbody">
              
              <tr>
                <td>Price</td>
                <td>Currency</td>
                <td>Description</td>
              </tr>

              <tr>
                <td><input type="number" name="diamond_price_a_1" placeholder="1st Price" class="form-control"></td>
                <td>
                    <select name="diamond_currency_a_1" class="form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="diamond_description_a_1" placeholder="1st Description" class="form-control"></td>
              </tr>

              <tr>
                <td><input type="number" name="diamond_price_a_2" placeholder="2st Price" class="form-control"></td>
                <td>
                    <select name="diamond_currency_a_2" class="form-control">
                      <option>TL</option>
                      <option>€</option>
                      <option>$</option>
                      <option>£</option>
                    </select>
                </td>
                <td><input type="text" name="diamond_description_a_2" placeholder="2nd Description" class="form-control"></td>
              </tr>

            </tbody>

            <tr>
              <td>Service Image(1) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage1" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(2) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage2" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(3) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage3" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(4) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage4" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(5) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage5" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(6) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage6" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(7) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage7" class="form-control"></td>
            </tr>

            <tr>
              <td>Service Image(8) <span class="red"></span></td>
              <td colspan="2"><input type="file" name="serviceImage8" class="form-control"></td>
            </tr>

            <tr>
              <td>
                <input type="hidden" name="serviceId" id="serviceId" class="serviceId">
                <input type="hidden" name="_token" value="{{ Session::token() }}">
              </td>
              <td><input type="submit" class="btn btn-primary form-control"></td>
            </tr>
          </table>

          </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- Service Images Modal -->
<div class="modal fade" id="serviceImagesModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Service Images</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <div id="serviceImagesInModal"></div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- View Service Modal Modal -->
<div class="modal fade" id="viewServiceDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Service Info</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        
        <table class="table table-bordered">
          <tr>
            <td>Service Category</td>       
            <td id="tableServiceCategory"></td> 
          </tr>

          <tr>
            <td>Service Title</td>
            <td id="tableServiceTitle"></td>
          </tr>

          <tr>
            <td>Service Info</td>
            <td id="tableServiceInfo"></td>
          </tr>
        </table>

        <table class="table table-bordered">
          
          <tr>
            <td colspan="3"><strong>Bronze</strong> <div style="float: right;" class="bronze_tbody_action" onclick="toggle_bronze();"> <span class="fa fa-arrow-up"></span> </div> </td>
          </tr>

          <tbody class="bronze_tbody">
            
            <tr>
              <td>Price</td>
              <td>Currency</td>
              <td>Description</td>
            </tr>

            <tr>
              <td id="bronze_price_a_1_view"></td>
              <td id="bronze_currency_a_1_view"></td>
              <td id="bronze_description_a_1_view"></td>
            </tr>

            <tr>
              <td id="bronze_price_a_2_view"></td>
              <td id="bronze_currency_a_2_view"></td>
              <td id="bronze_description_a_2"></td>
            </tr>

          </tbody>


          <tr>
            <td colspan="3"><strong>Silver</strong> <div style="float: right;" class="silver_tbody_action" onclick="toggle_silver();"> <span class="fa fa-arrow-down"></span> </div> </td>
          </tr>

          <tbody class="silver_tbody">
            
            <tr>
              <td>Price</td>
              <td>Currency</td>
              <td>Description</td>
            </tr>

            <tr>
              <td id="silver_price_a_1_view"></td>
              <td id="silver_currency_a_1_view"></td>
              <td id="silver_description_a_1_view"></td>
            </tr>

            <tr>
              <td id="silver_price_a_2_view"></td>
              <td id="silver_currency_a_2_view"></td>
              <td id="silver_description_a_2_view"></td>
            </tr>

          </tbody>


          <tr>
            <td colspan="3"><strong>Gold</strong> <div style="float: right;" class="gold_tbody_action" onclick="toggle_gold();"> <span class="fa fa-arrow-down"></span> </div> </td>
          </tr>

          <tbody class="gold_tbody">
            
            <tr>
              <td>Price</td>
              <td>Currency</td>
              <td>Description</td>
            </tr>

            <tr>
              <td id="gold_price_a_1_view"></td>
              <td id="gold_currency_a_1_view"></td>
              <td id="gold_description_a_1_view"></td>
            </tr>

            <tr>
              <td id="gold_price_a_2_view"></td>
              <td id="gold_currency_a_2_view"></td>
              <td id="gold_description_a_2_view"></td>
            </tr>

          </tbody>

          <tr>
            <td colspan="3"><strong>Diamond</strong> <div style="float: right;" class="diamond_tbody_action" onclick="toggle_diamond();"> <span class="fa fa-arrow-down"></span> </div> </td>
          </tr>

          <tbody class="diamond_tbody">
            
            <tr>
              <td>Price</td>
              <td>Currency</td>
              <td>Description</td>
            </tr>

            <tr>
              <td id="diamond_price_a_1_view"></td>
              <td id="diamond_currency_a_1_view"></td>
              <td id="diamond_description_a_1_view"></td>
            </tr>

            <tr>
              <td id="diamond_price_a_2_view"></td>
              <td id="diamond_currency_a_2_view"></td>
              <td id="diamond_description_a_2_view"></td>
            </tr>

          </tbody>

        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">

  var show_bronze_status = "TRUE";
  var show_silver_status = "FALSE";
  var show_gold_status = "FALSE";
  var show_diamond_status = "FALSE";

  //Hide Silver Tbody
  $(".silver_tbody").hide();
  $(".gold_tbody").hide();
  $(".diamond_tbody").hide();

  //Toggle Diamond
  function toggle_diamond()
  {
    if (show_diamond_status == "TRUE") {
        show_diamond_status = "FALSE";
        $(".diamond_tbody").hide();
        $(".diamond_tbody_action").html("<span class='fa fa-arrow-down'></span>");
        
    }else {
      show_diamond_status = "TRUE";
      $(".diamond_tbody").show();
      $(".diamond_tbody_action").html("<span class='fa fa-arrow-up'></span>");

    }
  }


  //Toggle Silver
  function toggle_gold()
  {
    if (show_gold_status == "TRUE") {
        show_gold_status = "FALSE";
        $(".gold_tbody").hide();
        $(".gold_tbody_action").html("<span class='fa fa-arrow-down'></span>");
        
    }else {
      show_gold_status = "TRUE";
      $(".gold_tbody").show();
      $(".gold_tbody_action").html("<span class='fa fa-arrow-up'></span>");

    }
  }


  //Toggle Silver
  function toggle_silver()
  {
    if (show_silver_status == "TRUE") {
        show_silver_status = "FALSE";
        $(".silver_tbody").hide();
        $(".silver_tbody_action").html("<span class='fa fa-arrow-down'></span>");
        
    }else {
      show_silver_status = "TRUE";
      $(".silver_tbody").show();
      $(".silver_tbody_action").html("<span class='fa fa-arrow-up'></span>");

    }
  }

  //Toggle Bronze
  function toggle_bronze()
  {
    if (show_bronze_status == "TRUE") {
        show_bronze_status = "FALSE";
        $(".bronze_tbody").hide();
        $(".bronze_tbody_action").html("<span class='fa fa-arrow-down'></span>");
        
    }else {
      show_bronze_status = "TRUE";
      $(".bronze_tbody").show();
      $(".bronze_tbody_action").html("<span class='fa fa-arrow-up'></span>");

    }
  }


  function fillImageOnModal(serviceId ,serviceImage1, serviceImage2, serviceImage3, serviceImage4, serviceImage5, serviceImage6, serviceImage7, serviceImage8) {


      $("#serviceImagesInModal").html("");

      if (serviceImage1 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/1.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage2 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/2.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage3 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/3.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage4 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/4.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage5 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/5.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage6 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/6.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage7 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/7.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }

      if (serviceImage8 == "TRUE") {
        $("#serviceImagesInModal").append("<img src='/go{{ $pF }}/storage/services/"+serviceId+"/8.jpg' style='width: 200px; height: 200px; margin: 16px;'>");  
      }
      

  }


  function viewServiceInfo(serviceId, serviceStatus, serviceCategory, serviceCategoryTitle, serviceTitle, serviceInfo, bronze_price_a_1, bronze_currency_a_1, bronze_description_a_1, bronze_price_a_2, bronze_currency_a_2, bronze_description_a_2, silver_price_a_1,
    silver_currency_a_1, silver_description_a_1, silver_price_a_2, silver_currency_a_2, silver_description_a_2, gold_price_a_1, gold_currency_a_1,
    gold_description_a_1, gold_price_a_2, gold_currency_a_2, gold_description_a_2, diamond_price_a_1, diamond_currency_a_1, diamond_description_a_1,
    diamond_price_a_2, diamond_currency_a_2, diamond_description_a_2) {


    $("#tableServiceCategory").html(serviceCategoryTitle);
    $("#tableServiceTitle").html(serviceTitle);
    $("#tableServiceInfo").html(serviceInfo);

    $("#bronze_price_a_1_view").html(bronze_price_a_1);
    $("#bronze_currency_a_1_view").html(bronze_currency_a_1);
    $("#bronze_description_a_1_view").html(bronze_description_a_1);
    $("#bronze_price_a_2_view").html(bronze_price_a_2);
    $("#bronze_currency_a_2_view").html(bronze_currency_a_2);
    $("#bronze_description_a_2").html(bronze_description_a_2);

    $("#silver_price_a_1_view").html(silver_price_a_1);
    $("#silver_currency_a_1_view").html(silver_currency_a_1);
    $("#silver_description_a_1_view").html(silver_description_a_1);
    $("#silver_price_a_2_view").html(silver_price_a_2);
    $("#silver_currency_a_2_view").html(silver_currency_a_2);
    $("#silver_description_a_2_view").html(silver_description_a_2);
      
    $("#gold_price_a_1_view").html(gold_price_a_1);
    $("#gold_currency_a_1_view").html(gold_currency_a_1);
    $("#gold_description_a_1_view").html(gold_description_a_1);
    $("#gold_price_a_2_view").html(gold_price_a_2);
    $("#gold_currency_a_2_view").html(gold_currency_a_2);
    $("#gold_description_a_2_view").html(gold_description_a_2);
      
    $("#diamond_price_a_1_view").html(diamond_price_a_1);
    $("#diamond_currency_a_1_view").html(diamond_currency_a_1);
    $("#diamond_description_a_1_view").html(diamond_description_a_1);
    $("#diamond_price_a_2_view").html(diamond_price_a_2);
    $("#diamond_currency_a_2_view").html(diamond_currency_a_2);
    $("#diamond_description_a_2_view").html(diamond_description_a_2);

  }


  function editServiceInfo(serviceId, serviceStatus, serviceCategory, serviceCategoryTitle, serviceTitle, serviceInfo, bronze_price_a_1, bronze_currency_a_1, bronze_description_a_1, bronze_price_a_2, bronze_currency_a_2, bronze_description_a_2, silver_price_a_1,
    silver_currency_a_1, silver_description_a_1, silver_price_a_2, silver_currency_a_2, silver_description_a_2, gold_price_a_1, gold_currency_a_1,
    gold_description_a_1, gold_price_a_2, gold_currency_a_2, gold_description_a_2, diamond_price_a_1, diamond_currency_a_1, diamond_description_a_1,
    diamond_price_a_2, diamond_currency_a_2, diamond_description_a_2) {

    
    $(".serviceId").val(serviceId);
    $(".serviceCategory").val(serviceCategory);

    $(".serviceTitle").val(serviceTitle);
    $(".serviceInfo").val(serviceInfo);
    
    $(".bronze_price_a_1").val(bronze_price_a_1);
    $(".bronze_currency_a_1").val(bronze_currency_a_1);
    $(".bronze_description_a_1").val(bronze_description_a_1);
    $(".bronze_price_a_2").val(bronze_price_a_2);
    $(".bronze_currency_a_2").val(bronze_currency_a_2); 
    $(".bronze_description_a_2").val(bronze_description_a_2);
    
    $(".silver_price_a_1").val(silver_price_a_1);
    $(".silver_currency_a_1").val(silver_currency_a_1);
    $(".silver_description_a_1").val(silver_description_a_1);
    $(".silver_price_a_2").val(silver_price_a_2);
    $(".silver_currency_a_2").val(silver_currency_a_2);
    $(".silver_description_a_2").val(silver_description_a_2);

    $(".gold_price_a_1").val(gold_price_a_1);
    $(".gold_currency_a_1").val(gold_currency_a_1);
    $(".gold_description_a_1").val(gold_description_a_1);
    $(".gold_price_a_2").val(gold_price_a_2);
    $(".gold_currency_a_2").val(gold_currency_a_2);
    $(".gold_description_a_2").val(gold_description_a_2); 
  
    $(".diamond_price_a_1").val(diamond_price_a_1); 
    $(".diamond_currency_a_1").val(diamond_currency_a_1); 
    $(".diamond_description_a_1").val(diamond_description_a_1);
    $(".diamond_price_a_2").val(diamond_price_a_2);
    $(".diamond_currency_a_2").val(diamond_currency_a_2);
    $(".diamond_description_a_2").val(diamond_description_a_2);

  }

  function deleteServiceInfo(serviceId)
  {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete Service!'
    }).then((result) => {
      if (result.value) {

        $.get('/admin/delete/service/'+serviceId, function (data) {

          var jsonData = JSON.parse(JSON.stringify(data));

          if (jsonData.status == "SUCCESS") {

            Swal.fire(
              'Service Deleted!',
              'Service Was Deleted Successfully',
              'success'
            ).then(function () {
              window.location.reload();
            });

          }else {
            Swal.fire(
              'Error!',
              'Failed to Delete Service. Please Try Again Later',
              'error'
            );         
          }

        });

      }
    })
  }
</script>

@endsection
