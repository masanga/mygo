<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'userId', 'categoryTitle', 'categoryInfo',
    ];


    protected $hidden = [
       
    ];
}
