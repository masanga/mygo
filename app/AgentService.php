<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentService extends Model
{
    protected $fillable = [
        'userId', 'agentId', 'serviceId',
    ];


    protected $hidden = [
       
    ];
}
