<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    protected $fillable = [
        'userId', 'os', 'browser', 'ip',
    ];


    protected $hidden = [
       
    ];
}
