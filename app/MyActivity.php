<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MyActivity extends Model
{
    protected $fillable = [
        'userId', 'activityTitle', 'activityInfo',
    ];


    protected $hidden = [
       
    ];
}
