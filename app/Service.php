<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'userId', 'agentId', 'serviceId',
    ];


    protected $hidden = [
       
    ];
}
