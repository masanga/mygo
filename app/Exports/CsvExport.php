<?php

namespace App\Exports;

use App\Booking;
use App\Service;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class CsvExport implements  FromQuery, WithHeadings, WithEvents, WithMapping
{
	use Exportable;

    public  $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    } 

	public function registerEvents(): array
	{
		$styleArray = [
			'font' => [
				'bold' => true,
				
			],

			'color' => [
				'green' => true,
				
			]
		];

		return [
			AfterSheet::class => function(AfterSheet $event) use ($styleArray)
			{
				$event->sheet->getStyle('A1:H1')->applyFromArray($styleArray);
				$event->sheet->setCellValue('G7', '=SUM(G2:G6')->applyFromArray($styleArray);
			},
		];
	}
	// public function sheets(): array
	// {
	// 	$sheets = [];

	// 	for ($month = 1; $month <= 12; $month++){
	// 		$sheets[] = new InvoicesPerMonthSheet($this->year, $month);
	// 	}

	// 	return $sheets;
	// }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {

       if ($this->request->csv == "all") {

            return Booking::orderBy('created_at', 'DESC');
        }

        if ($this->request->csv == "agents"){
           return Booking::where('agentId', $this->request->agents_csv);
        }

        if ($this->request->csv == "service"){
           return Booking::where('serviceSelectedId', $this->request->service_csv);
        }

        if ($this->request->csv == "date"){
            return Booking::where('serviceDateBooking', $this->request->date_csv);
        }

        //return Booking::orderBy('created_at','DESC');
    }

    // public function collection()
    // {
    //     return Booking::all();
    // }


    public function map($booking): array 
    {
    	return [
            Service::find($booking->serviceSelectedId)->serviceTitle,
            $booking->serviceDateBooking,
            $booking->nameOfBooking,
            $booking->phoneOfBooking,
            $booking->totalPrice,
            $booking->restPrice,
            $booking->notes,
    	];
    }

    public function headings(): array
    {
    	return [
    		'Service Name',
    		'Booking Date',
    		'Booking Name',
    		'Phone Number',
            'Total Price',
    		'Rest Price',
    		'Notes',
    	];
    }

    // public function columnFormats(): array
    // {
    // 	return [
    // 		'B' => NumberFormat::FORMAT_DATE_DDMMYYYY,
    // 		'C' => NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE,
    // 	];
    // }
}
