<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    protected $fillable = [

		'bookingStatus', 'userId', 'agentId', 'serviceSelectedId', 'audioFileUploaded', 'serviceDateBooking', 'numberOfPrice1', 
		
		'numberOfPrice2', 'nameOfBooking', 'phoneOfBooking', 'totalPrice', 'totalPriceCurrency', 'depositPaymentPrice', 

		'depositPaymentCurrency', 'restPrice', 'restCurrency', 'notes',
    ];


    protected $hidden = [
       
    ];
}
