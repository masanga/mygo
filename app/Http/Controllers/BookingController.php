<?php

namespace App\Http\Controllers;

use Storage;
use App\Booking;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{

    //searchSimilarNameOnBooking
    public function searchSimilarNameOnBooking(Request $request)
    {
        //Form Validation
        $this->validate($request,
            [
                'nameQuery' => 'required'
            ]);

        $similarNames = array();

        //Searching for similar Names
        $bookingSimilarNames = Booking::where([
            ['nameOfBooking', 'LIKE', '%'.$request->nameQuery.'%']
            ])->get();

        foreach ($bookingSimilarNames as $booking) {
            if (!in_array($booking->nameOfBooking, $similarNames)) {
                $similarNames[] = $booking->nameOfBooking;    
            }
        }

        return response()->json(array('data' => $similarNames), 200);
    }

    //audioUploadTest
    public function audioUploadTest(Request $request)
    {
        echo $request->name;

        //Uploading File
        Storage::disk('local')->putFileAs("audios/", $request->file('audio'), "file_1.webm");
    }

	//audioRecordTest
	public function audioRecordTest()
	{
		return view("tests.audioRecordTest");
	}

    //postNewBooking
    public function postNewBooking(Request $request)
    {
    	//Form Validation
    	$this->validate($request,
    		[
    			'agentId' => 'required',
                'serviceSelectedId' => 'required',
                'nameOfBooking' => 'required'
    		]);

        //Adding New Booking
        $newBooking = new Booking();
        $newBooking->bookingStatus = "NEW";
        $newBooking->userId = Auth::User()->id;
        $newBooking->agentId = $request->agentId;
        $newBooking->serviceSelectedId = $request->serviceSelectedId;
        $newBooking->serviceDateBooking = $request->serviceDateBooking;
        $newBooking->numberOfPrice1 = $request->numberOfPrice1;
        $newBooking->numberOfPrice2 = $request->numberOfPrice2;
        $newBooking->nameOfBooking = $request->nameOfBooking;
        $newBooking->phoneOfBooking = $request->phoneOfBooking;

        $newBooking->totalPrice = $request->totalPrice;
        $newBooking->totalPriceCurrency = $request->totalPriceCurrency;

        $newBooking->depositPaymentPrice = $request->depositPaymentPrice;
        $newBooking->depositPaymentCurrency = $request->depositPaymentCurrency;

        $newBooking->restPrice = $request->restPrice;
        $newBooking->restCurrency = $request->restCurrency;

        $newBooking->notes = $request->notes;


        if ($newBooking->save()) {
            //Uploading Audio Data if Present
            if ($request->file('audio')) {
                Storage::disk('local')->putFileAs("audios/", $request->file('audio'), "file_".$newBooking->id.".ogg");   
            }


            return response()->json(array('status' => 'SUCCESS'), 201);
        }else {
            return response()->json(array('status' => 'FAILURE'), 200);
        }
        
    }
}
