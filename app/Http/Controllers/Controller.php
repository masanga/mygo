<?php

namespace App\Http\Controllers;

use Hash;

use App\User;
use App\Booking;
use App\Service;
use App\Category;
use App\LoginLog;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    //postChangePassword
    public function postChangePassword(Request $request)
    {
        //Form Validation
        $this->validate($request,
            [
                'current_password' => 'required',
                'new_password' => 'required|confirmed'
            ]);

        //Check If Old Password is Correct
        if (Hash::check($request->current_password, Auth::User()->password)) {

            //Changing User Password
            $changePassword = User::find(Auth::User()->id);
            $changePassword->password = $request->new_password;

            if ($changePassword->update()) {

                //Add Activity Log
                app('App\Http\Controllers\MyActivityController')->addNewActivity('Security Action', 'Changed Account Password');

                $message = "Your Password Was Updated Successfully";
                return redirect()->back()->with(['successMessage' => $message]);
            }else {
                $message = "Failed to Update Your Password. Please Try Again Later";
                return redirect()->back()->with(['errorMessage' => $message]);
            }

        }else {
            $message = "Please Enter Correct Current Password";
            return redirect()->back()->with(['errorMessage' => $message]);
        }
    }

    //adminBookings
    public function adminBookings()
    {
        $bookings = Booking::orderBy('created_at', 'DESC')->paginate(24);

        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Admin Bookings Page');

        return view("pages.adminBookings",
                            [
                                'bookings' => $bookings
                            ]);
    }

    //deleteAgentInfo
    public function deleteAgentInfo($agentId)
    {
        $deleteAgentInfo = User::find($agentId)->delete();

        return response()->json(array('status' => 'SUCCESS'), 200);
    }

    //postUpdateAgentDetails
    public function postUpdateAgentDetails(Request $request)
    {
        //Form Validation
        $this->validate($request,
            [
                'agentFirstName' => 'required',
                'agentLastName' => 'required',
                'agentEmail' => 'required'
            ]);

        //Adding New Agent
        $updateAgent = User::find($request->agentIdOnEdit);
        $updateAgent->firstName = $request->agentFirstName;
        $updateAgent->lastName = $request->agentLastName;
        $updateAgent->email = $request->agentEmail;
        $updateAgent->phoneNumber = $request->agentPhoneNumber;
        
        if ($updateAgent->save()) {
            $message = "Agent Data Was Updated Successfully";
            return redirect()->back()->with(['successMessage' => $message]);
        }else {
            $message = "Failed to Update Agent Data. Please Try Again Later";
            return redirect()->back()->with(['errorMessage' => $message]);
        }

    }

    //postNewAgent
    public function postNewAgent(Request $request)
    {
        //Form Validation
        $this->validate($request,
            [
                'firstName' => 'required',
                'lastName' => 'required',
                'email' => 'required',
                'agentClass' => 'required',
                'password' => 'required|confirmed'
            ]);

        //Adding New Agent
        $newAgent = new User();
        $newAgent->firstName = $request->firstName;
        $newAgent->lastName = $request->lastName;
        $newAgent->email = $request->email;
        $newAgent->phoneNumber = $request->phoneNumber;
        $newAgent->agentClass = $request->agentClass;
        $newAgent->password = $request->password;
        $newAgent->level = 2;

        if ($newAgent->save()) {
            $message = "You Have Added New Agent Successfully";
            return redirect()->back()->with(['successMessage' => $message]);
        }else {
            $message = "Failed to Add New Agent. Please Try Again Later";
            return redirect()->back()->with(['errorMessage' => $message]);
        }

    }

    //Login Logs
    public function loginLogs()
    {

        $loginLogs = LoginLog::where('userId', Auth::User()->id)->orderBy('created_at', 'DESC')->paginate(12);

        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Login Logs Page');

        return view("auth_custom.login_logs",
                        [
                            'loginLogs' => $loginLogs
                        ]);
    }

    //Change Password
    public function changePassword()
    {
        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Change Password Page');

        return view("auth_custom.change_password");
    }

    //Payments
    public function reports()
    {
        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Reports Page');

    	return view("pages.reports");
    }

    //Bookings
    public function bookings()
    {
        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Bookings Page');

    	return view("pages.bookings");
    }

    //Tours
    public function services()
    {

        $servicesList = Service::orderBy('created_at', 'DESC')->paginate(12);
        $servicesTotal = Service::all()->count();
        $serviceCategories = Category::orderBy('categoryTitle', 'ASC')->get();

        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Services Page');

    	return view("pages.services",
                    [
                        'servicesList' => $servicesList,
                        'servicesTotal' => $servicesTotal,
                        'serviceCategories' => $serviceCategories
                    ]);
    }

    //Agents
    public function agents()
    {

        $agentsTotal = User::where('level', 2)->orderBy('created_at', 'DESC')->get()->count();
        $agents = User::where('level', 2)->orderBy('created_at', 'DESC')->paginate(12);

        $servicesList = Service::orderBy('created_at', 'DESC')->get();

        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Agents Page');

    	return view("pages.agents",
                        [
                            'agents' => $agents,
                            'agentsTotal' => $agentsTotal,
                            'servicesList' => $servicesList
                        ]);
    }

    //Index Page
    public function index()
    {
    	return view("index");
    }
}
