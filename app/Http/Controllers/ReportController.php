<?php

namespace App\Http\Controllers;

use PDF;
use Excel;
use App\User;
use App\Booking;
use App\Report;
use App\Service;
use Illuminate\Http\Request;
use App\Exports\CsvExport;
use App\Exports\BookingExport;
class ReportController extends Controller
{


	//postGenerateMembersReport
	public function servicesReportView(Request $request)
	{

	     return Excel::download(new BookingExport($request), 'Bookings Report.csv');
	   
	}

	 // Searching Agent Name
    public function searchAgentName(Request $request)
    {
        //Form Validation
        $this->validate($request,
            [
                'nameQuery' => 'required'
            ]);

        $similarNames = array();

        //Searching for similar Names
        $bookingSimilarNames = Booking::where([
            ['nameOfBooking', 'LIKE', '%'.$request->nameQuery.'%']
            ])->get();

        foreach ($bookingSimilarNames as $booking) {
            if (!in_array($booking->nameOfBooking, $similarNames)) {
                $similarNames[] = $booking->nameOfBooking;    
            }
        }

        return response()->json(array('data' => $similarNames), 200);
    }

	//generateBookingsReport
	public function generateCsvReport()
	{
		$agents = User::where('level', '=','2')->get();
    	$services = Service::orderBy('created_at', 'DESC')->get();

		return view("report.generateCsvReport",
						[
							'agents' => $agents,
							'services' => $services,
						]);
	}

	//generateBookingsReportView
	public function generateBookingsReportView(Request $request)
	{
	   return Excel::download(new CsvExport($request), 'Bookings CSV Report.csv');
	}

	//generateServicesReport
	public function generateExcelReport()
	{
		$agents = User::where('level', '=','2')->get();
    	$services = Service::orderBy('created_at', 'DESC')->get();

		return view("report.generateExcelReport",
						[
							'agents' => $agents,
							'services' => $services,
						]);
	}


	//generateServicesReportView
	// public function generateServicesReportView(Request $request)
	// {
	// 	return view("report.pdf.servicesView");
	// }

    //generateAgentsReport
    public function generatePdfReport()
    {
    	$agents = User::where('level', '=','2')->get();
    	$services = Service::orderBy('created_at', 'DESC')->get();

    	return view("report.generatePdfReport", ['agents' => $agents, 'services' => $services]);
    }

    //Get Report View
    public function generateAgentsReportView(Request $request)
    {
    	if ($request->pdf == "all") {

	           $serviceReports = Booking::orderBy('created_at', 'DESC')->get();

	           }

	    if ($request->pdf == "agents"){

		       $serviceReports = Booking::where('agentId', $request->agents_pdf)->get();
	           }

	    if ($request->pdf == "service"){

		       $serviceReports = Booking::where('serviceSelectedId', $request->service_pdf)->get();
	           }

	     if ($request->pdf == "date"){

	     	// echo $request->date_pdf;

	     	// exit();

		       $serviceReports = Booking::where('serviceDateBooking', $request->date_pdf)->get();
	           }

	    $pdf = PDF::loadView('report.pdf.agentsView',['serviceReports' => $serviceReports]);

    	return $pdf->download('Booking Report.pdf');
	           return view('report.pdf.agentsView',['serviceReports' => $serviceReports]);

    }

     //Download Pdf
    public function downloadPdf(Request $request)
    {

    	if ($request->service == "ALL") {

	           $serviceReports = Booking::orderBy('created_at', 'DESC')->get();

	           }else{

		       $serviceReports = Booking::where('serviceSelectedId', $request->service)->get();
	           }

    	$pdf = PDF::loadView('report.pdf.servicesView',['serviceReports' => $serviceReports]);
    	return $pdf->download('Booking Report.pdf');
    }
}
