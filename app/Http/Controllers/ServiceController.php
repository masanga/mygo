<?php

namespace App\Http\Controllers;

use Storage;

use App\Service;
use App\AgentService;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{

    //getServiceDetails
    public function getServiceDetails($serviceId)
    {
        return response()->json(Service::where('id', $serviceId)->get(), 200);
    }

    //addServiceToAgent
    public function addServiceToAgent($agentId, $serviceId) {
        $newServiceToAgent = new AgentService();
        $newServiceToAgent->userId = Auth::User()->id;
        $newServiceToAgent->agentId = $agentId;
        $newServiceToAgent->serviceId = $serviceId;

        if ($newServiceToAgent->save()) {
            return response()->json(array('status' => 'SUCCESS'), 201);
        }else {
            return response()->json(array('status' => 'FAILURE'), 200);
        }
    }

    //removeServiceToAgent
    public function removeServiceToAgent($agentId, $serviceId) {
        $agentServiceQuery = AgentService::where([
                                ['agentId', '=', $agentId],
                                ['serviceId', '=', $serviceId]
                            ])->get()->toArray();

        $deleteAgentService = AgentService::find($agentServiceQuery['0']['id'])->delete();

        return response()->json(array('status' => 'SUCCESS'), 200);

    }

    //agentServicesList
    public function agentServicesList($agentId)
    {

        $agentData = User::where('id', $agentId)->get()->toArray();
        $agentClass = $agentData['0']['agentClass'];

        $allServices = Service::orderBy('created_at', 'DESC')->get();

        $responseData = array();

        foreach ($allServices as $service) {
            $checkAgentService = AgentService::where([
                    ['agentId', '=', $agentId],
                    ['serviceId', '=', $service->id]
                ])->get();

            $serviceAdded = "";

            if (sizeof($checkAgentService) == 1) {
                $serviceAdded = "TRUE";
            }else {
                $serviceAdded = "FALSE";
            }


            $priceTitle1 = "";
            $priceAmount1 = "";
            $priceCurrency1 = "";

            $priceTitle2 = "";
            $priceAmount2 = "";
            $priceCurrency2 = "";

            if ($agentClass == "BRONZE") {
                $priceAmount1 = $service->bronze_price_a_1;
                $priceCurrency1 = $service->bronze_currency_a_1;

                $priceAmount2 = $service->bronze_price_a_2;
                $priceCurrency2 = $service->bronze_currency_a_2;

            }else if ($agentClass == "SILVER") {
                $priceAmount1 = $service->silver_price_a_1;
                $priceCurrency1 = $service->silver_currency_a_1;

                $priceAmount2 = $service->silver_price_a_2;
                $priceCurrency2 = $service->silver_currency_a_2;

            }else if ($agentClass == "GOLD") {
                $priceAmount1 = $service->gold_price_a_1;
                $priceCurrency1 = $service->gold_currency_a_1;

                $priceAmount2 = $service->gold_price_a_2;
                $priceCurrency2 = $service->gold_currency_a_2;

            }else if ($agentClass == "DIAMOND") {
                $priceAmount1 = $service->diamond_price_a_1;
                $priceCurrency1 = $service->diamond_currency_a_1;

                $priceAmount2 = $service->diamond_price_a_2;
                $priceCurrency2 = $service->diamond_currency_a_2;

            }

            $responseData[] = array('serviceTitle' => $service->serviceTitle, 
                                'serviceInfo' => $service->serviceInfo,
                                'serviceAdded' => $serviceAdded,

                                'serviceId' => $service->id,
                                'agentClass' => ucwords(strtolower($agentClass)),

                                'priceAmount1' => $priceAmount1,
                                'priceCurrency1' => $priceCurrency1,

                                'priceAmount2' => $priceAmount2,
                                'priceCurrency2' => $priceCurrency2
                                );
        }

        return response()->json(array('data' => $responseData), 200);
    }

    //postUpdateServiceData
    public function postUpdateServiceData(Request $request)
    {
        //Form Validation
        $this->validate($request,
            [
                'serviceId' => 'required',
                'serviceTitle' => 'required'
            ]);

            //Add New Service
            $updateService = Service::find($request->serviceId);
            $updateService->userId = Auth::User()->id;
            $updateService->serviceCategory = $request->serviceCategory;
            $updateService->serviceTitle = $request->serviceTitle;
            $updateService->serviceInfo = $request->serviceInfo;

            $updateService->bronze_price_a_1 = $request->bronze_price_a_1;
            $updateService->bronze_currency_a_1 = $request->bronze_currency_a_1;
            $updateService->bronze_description_a_1 = $request->bronze_description_a_1;
            $updateService->bronze_price_a_2 = $request->bronze_price_a_2;
            $updateService->bronze_currency_a_2 = $request->bronze_currency_a_2;
            $updateService->bronze_description_a_2 = $request->bronze_description_a_2;

            $updateService->silver_price_a_1 = $request->silver_price_a_1;
            $updateService->silver_currency_a_1 = $request->silver_currency_a_1;
            $updateService->silver_description_a_1 = $request->silver_description_a_1;
            $updateService->silver_price_a_2 = $request->silver_price_a_2;
            $updateService->silver_currency_a_2 = $request->silver_currency_a_2;
            $updateService->silver_description_a_2 = $request->silver_description_a_2;

            $updateService->gold_price_a_1 = $request->gold_price_a_1;
            $updateService->gold_currency_a_1 = $request->gold_currency_a_1;
            $updateService->gold_description_a_1 = $request->gold_description_a_1;
            $updateService->gold_price_a_2 = $request->gold_price_a_2;
            $updateService->gold_currency_a_2 = $request->gold_currency_a_2;
            $updateService->gold_description_a_2 = $request->gold_description_a_2;

            $updateService->diamond_price_a_1 = $request->diamond_price_a_1;
            $updateService->diamond_currency_a_1 = $request->diamond_currency_a_1;
            $updateService->diamond_description_a_1 = $request->diamond_description_a_1;
            $updateService->diamond_price_a_2 = $request->diamond_price_a_2;
            $updateService->diamond_currency_a_2 = $request->diamond_currency_a_2;
            $updateService->diamond_description_a_2 = $request->diamond_description_a_2;

            if ($updateService->save()) {

                //Uploading Service Image 1
                if ($request->file('serviceImage1')) {
                    $file = $request->file('serviceImage1');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "1.jpg");  

                    $this->updateServiceFileTable($updateService->id, "1"); 
                }
                

                //Uploading Service Image 2
                if ($request->file('serviceImage2')) {
                    $file = $request->file('serviceImage2');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "2.jpg");  

                    $this->updateServiceFileTable($updateService->id, "2"); 
                }

                //Uploading Service Image 3
                if ($request->file('serviceImage3')) {
                    $file = $request->file('serviceImage3');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "3.jpg");  

                    $this->updateServiceFileTable($updateService->id, "3"); 
                }

                //Uploading Service Image 4
                if ($request->file('serviceImage4')) {
                    $file = $request->file('serviceImage4');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "4.jpg");  

                    $this->updateServiceFileTable($updateService->id, "4"); 
                }


                //Uploading Service Image 5
                if ($request->file('serviceImage5')) {
                    $file = $request->file('serviceImage5');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "5.jpg");  

                    $this->updateServiceFileTable($updateService->id, "5"); 
                }

                //Uploading Service Image 6
                if ($request->file('serviceImage6')) {
                    $file = $request->file('serviceImage6');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "6.jpg");  

                    $this->updateServiceFileTable($updateService->id, "6"); 
                }

                //Uploading Service Image 7
                if ($request->file('serviceImage7')) {
                    $file = $request->file('serviceImage7');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "7.jpg");  

                    $this->updateServiceFileTable($updateService->id, "7"); 
                }

                //Uploading Service Image 8
                if ($request->file('serviceImage8')) {
                    $file = $request->file('serviceImage8');
                    Storage::disk('local')->putFileAs(pF().'/services/'.$updateService->id."/", $file, "8.jpg");  

                    $this->updateServiceFileTable($updateService->id, "8"); 
                }

                $message = "Service Was Updated Successfully.";
                return redirect()->back()->with(['successMessage' => $message]);

            }else {
                
                $message = "Failed to Update Service. Please Try Again Later";
                return redirect()->back()->with(['errorMessage' => $message]);

            }
    }

    //deleteService
    public function deleteService($serviceId)
    {
        $deleteService = Service::find($serviceId)->delete();

        
        return response()->json(array('status' => 'SUCCESS'), 200);
    }

    //postNewTour
    public function postNewService(Request $request)
    {
    	//Form Validation
    	$this->validate($request,
    			[
    				'serviceTitle' => 'required'
    			]);

    	//Add New Service
    	$newService = new Service();
        $newService->userId = Auth::User()->id;
        $newService->status = "TRUE";
        $newService->serviceCategory = $request->serviceCategory;
        $newService->serviceTitle = $request->serviceTitle;
        $newService->serviceInfo = $request->serviceInfo;

        $newService->bronze_price_a_1 = $request->bronze_price_a_1;
        $newService->bronze_currency_a_1 = $request->bronze_currency_a_1;
        $newService->bronze_description_a_1 = $request->bronze_description_a_1;
        $newService->bronze_price_a_2 = $request->bronze_price_a_2;
        $newService->bronze_currency_a_2 = $request->bronze_currency_a_2;
        $newService->bronze_description_a_2 = $request->bronze_description_a_2;

        $newService->silver_price_a_1 = $request->silver_price_a_1;
        $newService->silver_currency_a_1 = $request->silver_currency_a_1;
        $newService->silver_description_a_1 = $request->silver_description_a_1;
        $newService->silver_price_a_2 = $request->silver_price_a_2;
        $newService->silver_currency_a_2 = $request->silver_currency_a_2;
        $newService->silver_description_a_2 = $request->silver_description_a_2;

        $newService->gold_price_a_1 = $request->gold_price_a_1;
        $newService->gold_currency_a_1 = $request->gold_currency_a_1;
        $newService->gold_description_a_1 = $request->gold_description_a_1;
        $newService->gold_price_a_2 = $request->gold_price_a_2;
        $newService->gold_currency_a_2 = $request->gold_currency_a_2;
        $newService->gold_description_a_2 = $request->gold_description_a_2;

        $newService->diamond_price_a_1 = $request->diamond_price_a_1;
        $newService->diamond_currency_a_1 = $request->diamond_currency_a_1;
        $newService->diamond_description_a_1 = $request->diamond_description_a_1;
        $newService->diamond_price_a_2 = $request->diamond_price_a_2;
        $newService->diamond_currency_a_2 = $request->diamond_currency_a_2;
        $newService->diamond_description_a_2 = $request->diamond_description_a_2;
        
    	if ($newService->save()) {

            //Uploading Service Image 1
            if ($request->file('serviceImage1')) {
                $file = $request->file('serviceImage1');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "1.jpg");  

                $this->updateServiceFileTable($newService->id, "1"); 
            }
            

            //Uploading Service Image 2
            if ($request->file('serviceImage2')) {
                $file = $request->file('serviceImage2');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "2.jpg");  

                $this->updateServiceFileTable($newService->id, "2"); 
            }

            //Uploading Service Image 3
            if ($request->file('serviceImage3')) {
                $file = $request->file('serviceImage3');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "3.jpg");  

                $this->updateServiceFileTable($newService->id, "3"); 
            }

            //Uploading Service Image 4
            if ($request->file('serviceImage4')) {
                $file = $request->file('serviceImage4');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "4.jpg");  

                $this->updateServiceFileTable($newService->id, "4"); 
            }


            //Uploading Service Image 5
            if ($request->file('serviceImage5')) {
                $file = $request->file('serviceImage5');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "5.jpg");  

                $this->updateServiceFileTable($newService->id, "5"); 
            }

            //Uploading Service Image 6
            if ($request->file('serviceImage6')) {
                $file = $request->file('serviceImage6');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "6.jpg");  

                $this->updateServiceFileTable($newService->id, "6"); 
            }

            //Uploading Service Image 7
            if ($request->file('serviceImage7')) {
                $file = $request->file('serviceImage7');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "7.jpg");  

                $this->updateServiceFileTable($newService->id, "7"); 
            }

            //Uploading Service Image 8
            if ($request->file('serviceImage8')) {
                $file = $request->file('serviceImage8');
                Storage::disk('local')->putFileAs(pF().'/services/'.$newService->id."/", $file, "8.jpg");  

                $this->updateServiceFileTable($newService->id, "8"); 
            }

    		$message = "New Service Was Uploaded Successfully.";
    		return redirect()->back()->with(['successMessage' => $message]);

    	}else {
    		
    		$message = "Failed to Upload New Service. Please Try Again Later";
    		return redirect()->back()->with(['errorMessage' => $message]);

    	}
    }

    //updateServiceFileTable
    private function updateServiceFileTable($serviceId, $fileNumberId)
    {
        $updateFileData = Service::find($serviceId);

        if ($fileNumberId == 1) {
            $updateFileData->serviceImage1 = "TRUE";    

        }else if ($fileNumberId == 2) {
            $updateFileData->serviceImage2 = "TRUE";

        }else if ($fileNumberId == 3) {
            $updateFileData->serviceImage3 = "TRUE";
            
        }else if ($fileNumberId == 4) {
            $updateFileData->serviceImage4 = "TRUE";
            
        }else if ($fileNumberId == 5) {
            $updateFileData->serviceImage5 = "TRUE";
            
        }else if ($fileNumberId == 6) {
            $updateFileData->serviceImage6 = "TRUE";
            
        }else if ($fileNumberId == 7) {
            $updateFileData->serviceImage7 = "TRUE";
            
        }else if ($fileNumberId == 8) {
            $updateFileData->serviceImage8 = "TRUE";
            
        }
        
        $updateFileData->update();
    }
}
