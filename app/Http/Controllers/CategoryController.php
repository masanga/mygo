<?php

namespace App\Http\Controllers;

use App\User;
use App\Category;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

	//Update Category
	public function postUpdateCategory(Request $request)
	{
		//Form Validation
		$this->validate($request,
			[
				'categoryId' => 'required',
				'categoryTitle' => 'required'
			]);

		//Updating Category Data
		$updateCategory = Category::find($request->categoryId);
		$updateCategory->categoryTitle = $request->categoryTitle;
		$updateCategory->categoryInfo = $request->categoryInfo;

		if ($updateCategory->update()) {
			$message = "Category Was Updated Successfully";
			return redirect()->back()->with(['successMessage' => $message]);
		}else {
			$message = "Failed to Update Category. Please Try Again Later";
			return redirect()->back()->with(['errorMessage' => $message]);
		}
	}

	//Delete Category
	public function deleteCategory($categoryId)
	{
		$deleteCategory = Category::find($categoryId)->delete();

		return response()->json(array('status' => 'SUCCESS'), 200);
	}

	//Add New Category
	public function postNewCategory(Request $request)
	{
		//Form Validation
		$this->validate($request,
			[
				'categoryTitle' => 'required'
			]);

		//Adding New Category
		$newCategory = new Category();
		$newCategory->userId = Auth::User()->id;
		$newCategory->categoryTitle = $request->categoryTitle;
		$newCategory->categoryInfo = $request->categoryInfo;

		if ($newCategory->save()) {
			$message = "New Category Was Added Successfully";
			return redirect()->back()->with(['successMessage' => $message]);
		}else {
			$message = "Failed to Add New Category. Please Try Again Later";
			return redirect()->back()->with(['errorMessage' => $message]);
		}
	}

    //categories
    public function categories()
    {

    	$categories = Category::orderBy('created_at', 'DESC')->paginate(12);

    	return view("pages.categories",
    					[
    						'categories' => $categories
    					]);
    }
}
