<?php

namespace App\Http\Controllers;

use App\User;
use App\MyActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyActivityController extends Controller
{
    //addNewActivity
    public static function addNewActivity($activityTitle, $activityInfo)
    {
    	$newActivity = new MyActivity();
    	$newActivity->userId = Auth::User()->id;
    	$newActivity->activityTitle = $activityTitle;
    	$newActivity->activityInfo = $activityInfo;

    	if ($newActivity->save()) {
    		return response()->json(array('status' => 'SUCCESS'), 201);
    	}else {
    		return response()->json(array('status' => 'ERROR'), 200);
    	}
    }

}
