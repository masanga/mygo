<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginLogController extends Controller
{
    //Create New Login Log
    public function newLoginLog($userId, $os, $browser, $ip)
    {
    	$newLoginLog = new LoginLog();
    	$newLoginLog->userId = $userId;
    	$newLoginLog->os = $os;
    	$newLoginLog->browser = $browser;
    	$newLoginLog->ip = $ip;
    	$newLoginLog->save();
    }
}
