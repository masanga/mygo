<?php

namespace App\Http\Controllers;

use App\Service;
use App\User;
use App\MyActivity;
use App\Booking;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $totalServices = Service::where('status', "TRUE")->get()->count();
        $totalAgents = User::where('level', 2)->get()->count();
        $totalBookings = Booking::where('bookingStatus', "NEW")->get()->count();
        $totalPayments = 0;

        $recentActivities = MyActivity::orderBy('created_at', 'DESC')->limit(10)->get();

        //Add Activity Log
        app('App\Http\Controllers\MyActivityController')->addNewActivity('Page Navigation', 'Launched Dashboard Page');

        return view('home',
            [
                'totalServices' => $totalServices,
                'totalAgents' => $totalAgents,
                'totalBookings' => $totalBookings,
                'totalPayments' => $totalPayments,
                'recentActivities' => $recentActivities
            ]);
    }
}
