<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'userId', 'paymentTitle', 'paymentInfo', 'paidAmount',
    ];


    protected $hidden = [
       
    ];
}
