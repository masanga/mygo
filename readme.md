A Project from Adnani Ayvaci


Making a Simple Admin Panel
Web Design Posted Jan 16, 2021
I need a simple admin panel to accept bookings from my agents. it is not for the visitors.

it is for b2b.

there will be an admin panel,

there will be 1 admin. And some agents,

FOR AGENTS:

agents will enter user and pass,

then he/she will make reservations for the previously added tours, services etc.

there will be 3 subpages: 1- edit profile 2-list reservations 3-make reservation

very minimalist design.

and there will be a booking form,

fill in the form, and send booking.

Done!

FOR ADMIN:
admin will see the bookings,

admin will have these rights:
- add tours
- add agents
- make booking in the name of any agents
- list bookings
- list payments, income vs.
- export the filtered booking list into excel format.
- export the next day's tours list into simple text format ( he can filter by tour item )
- etc less